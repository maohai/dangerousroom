﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using GameClient.NetWork;


namespace ProxyServer
{
    public class Server
    {
        public LengthEncode lengthEncode;
        public LengthDecode lengthDecode;
        public Decode decode;
        public Encode encode;
        public HandlerCenter center;

        private UserTokenPool pool;
        private Socket serverSocket;
        private int maxClient;
        private Semaphore acceptClient;//代理服务器 资源服务器
        public Server(int max)
        {
            maxClient = max;
            pool = new UserTokenPool(maxClient);
            acceptClient = new Semaphore(maxClient, maxClient);
        }

        /// <summary>
        /// 启动服务器
        /// </summary>
        /// <param name="port">端口</param>
        public void Start(int port)
        {
            serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            for (int i = 0; i < maxClient; i++)
            {
                UserToken token = new UserToken();
                token.SetEncode(center,lengthEncode, lengthDecode, decode, encode);
                token.processSend = ProcessSend;
                token.processClose = ClientClose;
                token.receiveSAEA.Completed += IO_Compile;
                token.sendSAEA.Completed += IO_Compile;
                pool.Push(token);
            }

            serverSocket.Bind(new IPEndPoint(IPAddress.Any, port));
            serverSocket.Listen(10);
            Debug.Log("代理服务器启动成功");
            StartAccept(null);
        }

        /// <summary>
        /// 开始监听客户端连接
        /// </summary>
        /// <param name="e"></param>
        private void StartAccept(SocketAsyncEventArgs e)
        {
            if (e == null)
            {
                e = new SocketAsyncEventArgs();
                e.Completed += OnAcceptCompile;
            }
            else
            {
                e.AcceptSocket = null;
            }

            //是否挂起
            bool result = serverSocket.AcceptAsync(e);
            if (!result)
            {
                ProcessAccept(e);
            }
        }

        /// <summary>
        /// 客户端连接了
        /// </summary>
        /// <param name="e"></param>
        private void ProcessAccept(SocketAsyncEventArgs e)
        {
            // Debug.Log(e.AcceptSocket.RemoteEndPoint+"连接进来了");
            UserToken token = pool.Pop();
            token.clientSocket = e.AcceptSocket;
            center.ClientConnect(token);
            StartReceive(token);
            //信号量-1
            acceptClient.WaitOne();
            StartAccept(e);
        }

        /// <summary>
        /// 监听客户端连接上事件
        /// </summary>
        private void OnAcceptCompile(object sender, SocketAsyncEventArgs e)
        {
            ProcessAccept(e);
        }


        /// <summary>
        /// 开启客户端监听
        /// </summary>
        /// <param name="token"></param>
        private void StartReceive(UserToken token)
        {
            bool result = token.clientSocket.ReceiveAsync(token.receiveSAEA);
            if (!result)
            {
                ProcessReceive(token.receiveSAEA);
            }
        }


        /// <summary>
        /// 接收消息
        /// </summary>
        private void ProcessReceive(SocketAsyncEventArgs e)
        {
            UserToken token = e.UserToken as UserToken;
            //消息接收成功
            if (token.receiveSAEA.BytesTransferred > 0 && token.receiveSAEA.SocketError == SocketError.Success)
            {
                byte[] message = new byte[token.receiveSAEA.BytesTransferred];
                Buffer.BlockCopy(token.receiveSAEA.Buffer, 0, message, 0, token.receiveSAEA.BytesTransferred);
                token.OnReceive(message);
                StartReceive(token);
            }
            else
            {
                if (token.receiveSAEA.SocketError != SocketError.Success)
                {
                    ClientClose(token, token.receiveSAEA.SocketError.ToString());
                }
                else
                {
                    ClientClose(token, "客户端主动断开连接");
                }
            }
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        private void ProcessSend(SocketAsyncEventArgs e)
        {
            UserToken token = e.UserToken as UserToken;
            if (e.SocketError != SocketError.Success)
            {
                ClientClose(token, e.SocketError.ToString());
            }
            else
            {
                token.Writed();
            }
        }

        /// <summary>
        /// 监听接收发送
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IO_Compile(object sender, SocketAsyncEventArgs e)
        {
            if (e.LastOperation == SocketAsyncOperation.Receive)
            {
                ProcessReceive(e);
            }
            else
            {
                ProcessSend(e);
            }
        }

        /// <summary>
        /// 客户端关闭
        /// </summary>
        /// <param name="token"></param>
        /// <param name="msg"></param>
        private void ClientClose(UserToken token, string msg)
        {
            if (token.clientSocket != null)
            {
                lock (token)
                {
                    // Debug.Log(token.clientSocket.RemoteEndPoint+"退出了");
                    center.ClientClose(token,msg);
                    token.Close();
                    pool.Push(token);
                    //信号量+1
                    acceptClient.Release();
                }
            }
        }
    }
}