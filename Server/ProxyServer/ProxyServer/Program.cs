﻿using System;
using GameClient.NetWork;

namespace ProxyServer
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            new Server(200)
            {
                lengthDecode = LengthEncoding.Decode,
                encode = MessageEncoding.Encode,
                decode = MessageEncoding.Decode,
                center = new HandlerCenter()
            }.Start(6666);
            while (true)
            {
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.Q)
                {
                    break;
                }
            }
        }
    }
}