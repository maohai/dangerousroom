﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using GameClient.NetWork;

namespace ProxyServer
{
    public class UserToken
    {
        public delegate void ProcessSend(SocketAsyncEventArgs e);

        public delegate void ProcessClose(UserToken token, string msg);

        internal ProcessSend processSend;
        internal ProcessClose processClose;

        public Socket clientSocket;
        public SocketAsyncEventArgs receiveSAEA;
        public SocketAsyncEventArgs sendSAEA;
        public string Ip => clientSocket?.RemoteEndPoint.ToString();
        public int area;


        private LengthEncode _lengthEncode;
        private LengthDecode _lengthDecode;
        private Decode _decode;
        private Encode _encode;
        private HandlerCenter _center;

        private List<byte> cache = new List<byte>();
        private bool isReading = false;
        private Queue<byte[]> writeQueue = new Queue<byte[]>();
        private bool isWriting = false;
        public UserToken()
        {
            receiveSAEA = new SocketAsyncEventArgs();
            sendSAEA = new SocketAsyncEventArgs();
            receiveSAEA.SetBuffer(new byte[1024], 0, 1024);
            receiveSAEA.UserToken = this;
            sendSAEA.UserToken = this;
        }

        public void SetEncode(HandlerCenter center,LengthEncode le, LengthDecode ld, Decode decode, Encode encode)
        {
            _lengthEncode = le;
            _lengthDecode = ld;
            _decode = decode;
            _encode = encode;
            _center=center;
        }

        /// <summary>
        /// 接收的消息
        /// </summary>
        public void OnReceive(byte[] data)
        {
            if (!isReading)
            {
                isReading = true;
                cache.AddRange(data);
                OnData();
            }
        }

        private void OnData()
        {
            byte[] buff = null;
            if (_lengthDecode != null)
            {
                buff = _lengthDecode(ref cache);
                if (buff == null)
                {
                    isReading = false;
                    return;
                }
            }
            else
            {
                if (cache.Count == 0)
                {
                    isReading = false;
                    return;
                }
            }

            if (_decode == null)
            {
                throw new Exception("消息解码器不能为空");
            }

            try
            {
                SocketModel message = _decode(buff);
                _center.MessageReceive(this, message);
            }
            catch (Exception e)
            {
                Debug.LogError("消息解析失败"+e.Message);
            }
            OnData();
        }

        public void Write(byte[] value)
        {
            if (clientSocket == null)
            {
                processClose(this, "客户端断开连接了");
                return;
            }

            writeQueue.Enqueue(value);
            if (!isWriting)
            {
                isWriting = true;
                OnWrite();
            }
        }

        private void OnWrite()
        {
            if (writeQueue.Count == 0)
            {
                isWriting = false;
                return;
            }

            byte[] buff = writeQueue.Dequeue();
            sendSAEA.SetBuffer(buff, 0, buff.Length);
            bool result = clientSocket.SendAsync(sendSAEA);
            if (!result)
            {
                processSend(sendSAEA);
            }
        }

        public void Writed()
        {
            OnWrite();
        }

        /// <summary>
        /// 关闭客户端连接
        /// </summary>
        public void Close()
        {
            try
            {
                writeQueue.Clear();
                isReading = false;
                isWriting = false;
                cache.Clear();
                clientSocket.Shutdown(SocketShutdown.Both);
                clientSocket.Close();
                clientSocket = null;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                throw;
            }
        }
    }
}