﻿using System.Collections.Generic;
using GameClient.NetWork;

namespace ProxyServer
{
    public class HandlerCenter
    {
        private Dictionary<int, UserToken> serverTokens1 = new Dictionary<int, UserToken>();
        private Dictionary<UserToken, int> serverTokens2 = new Dictionary<UserToken, int>();
        
        private Dictionary<int, List<UserToken>> clientTokens = new Dictionary<int, List<UserToken>>();
        /// <summary>
        /// 客户端连接
        /// </summary>
        /// <param name="token">连接客户端对象</param>
        public void ClientConnect(UserToken token)
        {
            // Debug.Log(token.clientSocket.RemoteEndPoint);
            SocketModel connectModel = new SocketModel()
            {
                ipSelf = token.clientSocket.LocalEndPoint.ToString(),
                ipTarget = token.clientSocket.RemoteEndPoint.ToString(),
                message = new MsgDto(token.clientSocket.RemoteEndPoint.ToString())
            };
            // Debug.Log(connectModel);
            Write(token, connectModel);
        }

        /// <summary>
        /// 收到客户端消息
        /// </summary>
        /// <param name="token">发送消息的客户端对象</param>
        /// <param name="message">消息内容</param>
        public void MessageReceive(UserToken token, SocketModel message)
        {
            int area = message.area;
            if ((int)message.type == 0) //服务器连接上来了
            {
                Debug.Log($"服务{area}连接成功");
                serverTokens1.Add(area, token);
                serverTokens2.Add(token, area);
                return;
            }
            if (serverTokens2.ContainsKey(token))
            {
                ServerReceive(token, message);
            }
            else
            {
                ClientReceive(token,message);
            }
        }

        /// <summary>
        /// 服务器发来的消息
        /// </summary>
        /// <param name="token"></param>
        /// <param name="message"></param>
        private void ServerReceive(UserToken token,SocketModel message)
        {
            int area = message.area;
            Debug.Log($"Server{area}--{message.ipTarget}:{message}");
            UserToken client = GetClientUser(area, message.ipTarget);
            if (client != null)
            {
                Write(client, message);
            }
            else
            {
                Debug.LogError($"客户端{message.ipTarget}已断开连接");
                Write(token,new SocketModel(){ipSelf = message.ipTarget,ipTarget = message.ipSelf,type = PropType.CLOSE});
            }
        }

        /// <summary>
        /// 客户端发来的消息
        /// </summary>
        /// <param name="token"></param>
        /// <param name="message"></param>
        private void ClientReceive(UserToken token,SocketModel message)
        {
            int area = message.area;
            if (message.type == PropType.CONNECT)//客户端连接进来了
            {
                if (serverTokens1.ContainsKey(area))
                {
                    Debug.Log($"客户端{message.ipSelf} 连接服务器{area}");
                    Write(serverTokens1[area],message);
                    AddClient(area, token);
                }
                else
                {
                    Debug.LogError("所请求的服务器连接关闭了");
                }
            }
            else
            {
                if (serverTokens1.ContainsKey(area)) //有对应服务器，发给对应服务器处理
                {
                    Debug.Log($"Client{message.ipSelf}--Server{area}:{message}");
                    Write(serverTokens1[area], message);
                }
            }
        }

        /// <summary>
        /// 客户端断开连接
        /// </summary>
        /// <param name="token">断开的客户端对象</param>
        /// <param name="error">断开的信息</param>
        public void ClientClose(UserToken token, string error)
        {
            if (serverTokens2.ContainsKey(token))
            {
                var k = serverTokens2[token];
                serverTokens2.Remove(serverTokens1[k]);
                serverTokens1.Remove(k);
                Debug.Log($"服务器{k}断开连接了");
            }
            else
            {
                var model = new SocketModel();
                model.ipSelf = token.Ip;
                model.ipTarget = token.clientSocket.LocalEndPoint.ToString();
                model.type = PropType.CLOSE;
                
                Write(serverTokens1[token.area],model);
                RemoveClient(token);
            }
        }

        private void Write(UserToken token, SocketModel model)
        {
            byte[] ba = MessageEncoding.Encode(model);
            ba=LengthEncoding.Encode(ba);
            token.Write(ba);
        }

        private void AddClient(int area,UserToken token)
        {
            token.area = area;
            if (clientTokens.ContainsKey(area))
            {
                clientTokens[area].Add(token);
            }
            else
            {
                clientTokens.Add(area,new List<UserToken>(){token});
            }
        }

        private void RemoveClient(UserToken token)
        {
            int area = token.area;
            if (clientTokens.ContainsKey(area))
            {
                clientTokens[area].Remove(token);
                Debug.Log("客户端"+token.Ip+"关闭连接");
            }
        }

        private UserToken GetClientUser(int area, string clientIp)
        {
            UserToken userToken=null;
            if (clientTokens.ContainsKey(area))
            {
                var d = clientTokens[area];
                userToken = d.Find((token => token.Ip == clientIp));
            }
            return userToken;
        }
    }
}