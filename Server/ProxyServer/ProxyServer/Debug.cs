﻿using System;
using System.IO;

namespace ProxyServer
{
    public static class Debug
    {
        public static void LogError(object msg)
        {
            string m = DateTime.Now + " " + msg + "ERROR";
            Console.WriteLine(m);
            File.WriteAllText("./server.log",m);
        }

        public static void Log(object msg)
        {
            string m = DateTime.Now + " " + msg + "LOG";
            Console.WriteLine(m);
            File.WriteAllText("./server.log",m);
        }

        public static void LogInfo(object msg)
        {
            string m = DateTime.Now + " " + msg + "INFO";
            Console.WriteLine(m);
            File.WriteAllText("./server.log",m);
        }
    }
}