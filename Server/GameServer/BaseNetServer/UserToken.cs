using System.Net.Sockets;

namespace BaseNetServer
{
    public class UserToken
    {
        public string IP => ip;
        private string ip;
        public long MsgID;
        private Socket serverSocket;

        public UserToken(Socket socket)
        {
            MsgID = 0;
            serverSocket = socket;
        }

        public void SetIp(string ip)
        {
            this.ip = ip;
        }

        public void Write(byte[] value)
        {
            serverSocket.Send(value);
        }

        /// <summary>
        /// 关闭客户端连接
        /// </summary>
        public void Close()
        {
            MsgID = 0;
        }
    }
}