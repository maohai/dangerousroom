using System.Collections.Generic;

namespace BaseNetServer
{
    public class UserTokenPool
    {
        private Stack<UserToken> pool;
        /// <summary>
        /// 初始化池并创建所需池对象
        /// </summary>
        /// <param name="max"></param>
        public UserTokenPool(int max)
        {
            pool = new Stack<UserToken>(max);
        }

        /// <summary>
        /// 从池中获取客户端对象
        /// </summary>
        /// <returns></returns>
        public UserToken Pop()
        {
            return pool.Pop();
        }

        /// <summary>
        /// 客户端回收
        /// </summary>
        /// <param name="userToken"></param>
        public void Push(UserToken userToken)
        {
            if (userToken != null)
            {
                pool.Push(userToken);
            }
        }
    }
}