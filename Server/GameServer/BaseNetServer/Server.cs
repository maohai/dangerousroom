using System;
using System.Collections.Generic;
using System.Net.Sockets;
using GameClient.NetWork;

namespace BaseNetServer
{
    public class Server
    {
        public AdsHandlerCenter center;
        
        private UserTokenPool pool;
        private Socket serverSocket;
        private int maxClient;
        private byte[] readBuff = new byte[1024];
        private bool isReading = false;
        private string id;
        private List<byte> cache = new List<byte>();

        private Dictionary<string, UserToken> clientToken = new Dictionary<string, UserToken>();
        private int area;
        public Server(int max,int area)
        {
            maxClient = max;
            pool = new UserTokenPool(maxClient);
            this.area = area;

        }
        /// <summary>
        /// 启动服务器
        /// </summary>
        public void Start(string serverIp,int port)
        {
            serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            serverSocket.Connect(serverIp,port);
            serverSocket.BeginReceive(readBuff, 0, 1024, SocketFlags.None, ReceiveCallBack, readBuff);
            Debug.Log("代理服务器连接成功");
            //连接用户池
            for (int i = 0; i < maxClient; i++)
            {
                UserToken token = new UserToken(serverSocket);
                pool.Push(token);
            }
            center.ConnectSuccess(serverSocket.LocalEndPoint.ToString());
            //发送自身ip连接成功
            ByteArray arr = new ByteArray();
            byte[] ba = MessageEncoding.Encode(new SocketModel(){ipSelf = serverSocket.LocalEndPoint.ToString(),ipTarget = serverSocket.RemoteEndPoint.ToString(),area = area,command = 0,id=0,message = new MsgDto(false,serverSocket.LocalEndPoint.ToString())});
            arr.Write(ba.Length);
            arr.Write(ba);
            serverSocket.Send(arr.GetBuffer());
        }
        
        private void ReceiveCallBack(IAsyncResult ar)
        {
            int length = serverSocket.EndReceive(ar);
            byte[] message = new byte[length];
            
            Buffer.BlockCopy(readBuff, 0, message, 0, length);
            cache.AddRange(message);
            if (!isReading)
            {
                isReading = true;
                OnData();
                serverSocket.BeginReceive(readBuff, 0, 1024, SocketFlags.None, ReceiveCallBack, readBuff);
            }
        }
        
        private void OnData()
        {
            byte[] buff = null;
            buff = LengthEncoding.Decode(ref cache);
            if (buff == null)
            {
                isReading = false;
                return;
            }
            SocketModel message = MessageEncoding.Decode(buff);
            if (message == null)
            {
                isReading = false;
                return;
            }

            if (message.type == PropType.CONNECT)
            {
                OnConnect(message.ipSelf);
            }
            else if (message.type == PropType.CLOSE)
            {
                OnClose(message.ipSelf);
            }
            else
            {
                OnReceive(message.ipSelf,message);
            }
            OnData();
        }

        /// <summary>
        /// 客户端有连接时调用
        /// </summary>
        private void OnConnect(string ip)
        {
            UserToken token = pool.Pop();
            token.SetIp(ip);
            if (!clientToken.ContainsKey(ip))
            {
                clientToken.Add(ip,token);
            }
            center.ClientConnect(token);
        }

        private void OnReceive(string ip, SocketModel model)
        {
            if (clientToken.ContainsKey(ip))
            {
                center.MessageReceive(clientToken[ip],model);
            }
        }

        private void OnClose(string ip)
        {
            if (clientToken.ContainsKey(ip))
            {
                center.ClientClose(clientToken[ip],"");
                clientToken.Remove(ip);
            }
        }

        private void Write(UserToken token,SocketModel model)
        {
            ByteArray arr = new ByteArray();
            byte[] ba = MessageEncoding.Encode(model);
            arr.Write(ba.Length);
            arr.Write(ba);
            
            token.Write(arr.GetBuffer());
        }
        
    }
}