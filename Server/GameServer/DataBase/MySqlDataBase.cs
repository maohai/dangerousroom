using System;
using System.Collections.Generic;
using System.Reflection;
using DataBase.Model;
using DataBase.Order;
using MySql.Data.MySqlClient;

namespace DataBase
{
    internal class MySqlDataBase:BaseDataBase
    {
        private static MySqlDataBase _ins;
        private MySqlConnection _connection;
        private MySqlDataBase()
        {
        }
        public static MySqlDataBase Ins
        {
            get
            {
                if (_ins == null)
                {
                    _ins = new MySqlDataBase();
                }
                return _ins;
            }
        }


        public void Test()
        {
            // CreateTable(string.account,typeof(AccountModel));
        }
        
        public override void Connect(string name)
        {
            if (_connection == null)
            {
                string connStr = $"datasource={DataBaseConfig.datasource};port={DataBaseConfig.port};user={DataBaseConfig.user};pwd={DataBaseConfig.pwd}";
                _connection = new MySqlConnection(connStr);
                _connection.Open();
            }
            try
            {
                _connection.ChangeDatabase(name);
                Console.WriteLine("连接数据库"+name);
            }
            catch (MySqlException e)
            {
                Console.WriteLine("没有数据库"+name+"创建一个");
                string cmdText = "create database if not exists " + name;
                MySqlCommand cmd = new MySqlCommand(cmdText,_connection);
                cmd.ExecuteNonQuery();
                cmdText = "use " + name;
                cmd = new MySqlCommand(cmdText, _connection);
                cmd.ExecuteNonQuery();
            }
        }
        
        public override void CreateTable(string name, Type modelType)
        {
            //先查找是否有该表
            string cmdText = $"DESC {name}";
            try
            {
                MySqlCommand c = new MySqlCommand(cmdText, _connection);
                var r = c.ExecuteReader();
                Dictionary<string, DataInfo> dataInfos = GetDataInfosByType(modelType);
                List<string> needDelete = new List<string>();
                while (r.Read())
                {
                    string dataName = r["Field"].ToString();
                    if (dataInfos.ContainsKey(dataName))//表中已经有了
                    {
                        dataInfos.Remove(dataName);
                    }
                    else
                    {
                        needDelete.Add(dataName);
                    }
                }
                r.Dispose();
                r.Close();
                string dropCmd = "";
                //删除被清掉的数据 需谨慎
                // if (needDelete.Count > 0)
                // {
                //     foreach (var s in needDelete)
                //     {
                //         dropCmd += $"ALTER TABLE {name} DROP {s};";
                //     }
                // }
                if (dataInfos.Count > 0)
                {
                    foreach (var keyValuePair in dataInfos)
                    {
                        var info = keyValuePair.Value;
                        string df = info.Default == null ? "" : $"DEFAULT {info.Default}";
                        dropCmd += $"ALTER TABLE {name} ADD {keyValuePair.Key} {info.GetDataType()} {info.GetIsNULL()} {df};";
                    }
                }
                if (dropCmd != "")
                {
                    MySqlCommand c2 = new MySqlCommand(dropCmd, _connection);
                    c2.ExecuteNonQuery();
                    Console.WriteLine("更新表"+name);
                }
            }
            catch (MySqlException e)//表不存在
            {
                var tableName = GetInitModelFields(modelType);
                cmdText = @"CREATE TABLE IF NOT EXISTS `" + name+ "`"+ tableName +
                          " ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4;";
                try
                {
                    MySqlCommand cmd = new MySqlCommand(cmdText, _connection);
                    cmd.ExecuteNonQuery();
                    Console.WriteLine("创建表"+name);
                }
                catch (MySqlException ce)
                {
                    // Console.WriteLine(ce);
                }
            }
        }

        public override bool Insert(string tableName, IDataModel model)
        {
            try
            {
                var values = Merge(GetModelFieldValues(model),",");
                string cmdText = $"INSERT INTO `{tableName}` ({Merge(GetModelFields(model.GetType()),",")}) VALUES ({values});";
                MySqlCommand cmd = new MySqlCommand(cmdText, _connection);
                int result = cmd.ExecuteNonQuery();
                return result>0;
            }
            catch (MySqlException e)
            {
                return false;
            }
        }

        public override void DropTable(string name)
        {
            var cmdText = $"DROP TABLE {name}";
            try
            {
                MySqlCommand cmd = new MySqlCommand(cmdText, _connection);
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e);
            }
        }

        public override List<T> Select<T>(string tableName,Condition condition=null)
        {
            string cmdText = "select * from " + tableName;
            if (condition!=null)
            {
                cmdText +="WHERE " + condition; 
            }
            
            List<T> datas = new List<T>();
            try
            {
                MySqlCommand cmd = new MySqlCommand(cmdText, _connection);
                var r = cmd.ExecuteReader();
                List<DataInfo> dataInfos = new List<DataInfo>();
                List<FieldInfo> fieldInfos = new List<FieldInfo>();
                var fields = typeof(T).GetFields();
                foreach (var fieldInfo in fields)
                {
                    var dataInfo = fieldInfo.GetCustomAttribute<DataInfo>();
                    if (dataInfo != null)
                    {
                        dataInfos.Add(dataInfo);
                        fieldInfos.Add(fieldInfo);
                    }
                }
                while (r.Read())
                {
                    int i = 0;
                    T t = Activator.CreateInstance<T>();
                    foreach (var dataInfo in dataInfos)
                    {
                        fieldInfos[i].SetValue(t,r[dataInfo.DataName]);
                        i++;
                    }
                    datas.Add(t);
                }
                r.Dispose();
                r.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return datas;
        }
        public override bool Update(string tableName, IDataModel model)
        {
            var data = GetModelData(model);
            string msg = "";
            string key = GetKey(model.GetType());
            int i = 0;
            foreach (var keyValuePair in data)
            {
                msg += keyValuePair.Key + "=" + keyValuePair.Value;
                if (i < data.Count - 1)
                {
                    msg += ",";
                }
                i++;
            }
            string cmdText = $"UPDATE {tableName} SET {msg} WHERE {key}={data[key]}";
            try
            {
                MySqlCommand cmd = new MySqlCommand(cmdText, _connection);
                int result = cmd.ExecuteNonQuery();
                return result>0;
            }
            catch (MySqlException e)
            {
                return false;
            }
        }

        public override bool Delete(string tableName, Condition condition=null)
        {
            string cmdText = $"DELETE FROM {tableName} ";
            if (condition!=null)
            {
                cmdText +="WHERE " + condition; 
            }
            try
            {
                Console.WriteLine(cmdText);
                MySqlCommand cmd = new MySqlCommand(cmdText, _connection);
                int result = cmd.ExecuteNonQuery();
                // Console.WriteLine(result);
                return result>0;
            }
            catch (MySqlException e)
            {
                Console.WriteLine("删除失败"+e.Message);
                return false;
            }
        }
        
        public override void Close()
        {
            if(_connection!=null)
            {
                _connection.Dispose();
                _connection.Close();
            }
        }


        private string GetInitModelFields(Type modelType)
        {
            var fields = modelType.GetFields();
            string key = "";
            string cmd = "(";
            foreach (var fieldInfo in fields)
            {
                var info = fieldInfo.GetCustomAttribute<DataInfo>();
                if (info != null)
                {
                    string df = info.Default == null ? "," : $"DEFAULT {info.Default},";
                    cmd+=$"`{info.DataName}` {info.GetDataType()} {info.GetIsNULL()} "+df;
                    if (info.IsKey)
                    {
                        key = info.DataName;
                    }
                }
            }
            cmd += $"PRIMARY KEY(`{key}`))";
            return cmd;
        }

        private string GetKey(Type modelType)
        {
            var fields = modelType.GetFields();
            string key = "";
            foreach (var fieldInfo in fields)
            {
                var info = fieldInfo.GetCustomAttribute<DataInfo>();
                if (info != null)
                {
                    if (info.IsKey)
                    {
                        key = info.DataName;
                    }
                }
            }
            return $"`{key}`";
        }

        /// <summary>
        /// 获取字段
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private List<string> GetModelFields(Type model)
        {
            var fields = model.GetFields();
            List<string> values = new List<string>();
            foreach (var fieldInfo in fields)
            {
                var info = fieldInfo.GetCustomAttribute<DataInfo>();
                if (info != null)
                {
                    var value = info.DataName;
                    values.Add($"`{value}`");
                }
            }
            return values;
        }

        private string Merge(List<string> datas,string symbol)
        {
            string str = "";
            for (var i = 0; i < datas.Count; i++)
            {
                str += datas[i];
                if (i < datas.Count - 1)
                {
                    str += symbol;
                }
            }

            return str;
        }
        
        /// <summary>
        /// 获取字段值
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private List<string> GetModelFieldValues(IDataModel model)
        {
            var fields = model.GetType().GetFields();
            List<string> values = new List<string>();
            foreach (var fieldInfo in fields)
            {
                var info = fieldInfo.GetCustomAttribute<DataInfo>();
                if (info != null)
                {
                    var value = fieldInfo.GetValue(model);
                    values.Add($"'{value}'");
                }
            }
            return values;
        }

        private Dictionary<string, string> GetModelData(IDataModel model)
        {
            var fields = model.GetType().GetFields();
            Dictionary<string, string> data = new Dictionary<string, string>();
            foreach (var fieldInfo in fields)
            {
                var info = fieldInfo.GetCustomAttribute<DataInfo>();
                if (info != null)
                {
                    var value = fieldInfo.GetValue(model);
                    data.Add($"`{info.DataName}`",$"'{value}'");
                }
            }
            return data;
        }
    }
}