using System.Collections.Generic;

namespace DataBase.Order
{
    public enum CompareType
    {
        /// <summary>
        /// 等于
        /// </summary>
        Equal,
        /// <summary>
        /// 不等于
        /// </summary>
        NE,
        /// <summary>
        /// 大于
        /// </summary>
        G,
        /// <summary>
        /// 小于
        /// </summary>
        L,
        /// <summary>
        /// 大于等于
        /// </summary>
        GE,
        /// <summary>
        /// 小于等于
        /// </summary>
        LE,
        
        /// <summary>
        /// 匹配
        /// </summary>
        LIKE,
        
        /// <summary>
        /// 正则匹配
        /// </summary>
        REGEXP,
        
    }
    
    public enum LinkType
    {
        /// <summary>
        /// 并且
        /// </summary>
        AND,
        /// <summary>
        /// 或者
        /// </summary>
        OR,

    }
    
    public class Compare
    {
        private string str;
        public Compare(object a,object b,CompareType compareType)
        {
            str = $"{a} {GetStrByCompareType(compareType)} {b}";
        }

        public override string ToString()
        {
            return str;
        }

        private string GetStrByCompareType(CompareType compareType)
        {
            switch (compareType)
            {
                case CompareType.Equal:
                    return "=";
                case CompareType.G:
                    return ">";
                case CompareType.L:
                    return "<";
                case CompareType.GE:
                    return ">=";
                case CompareType.NE:
                    return "!=";
                case CompareType.LE:
                    return "<=";
                case CompareType.LIKE:
                    return "LIKE";
                case CompareType.REGEXP:
                    return "REGEXP";
            }

            return "=";
        }
    }

    public class Condition
    {
        private string str = "";

        public Compare a;

        public Compare b;

        public Compare c;
        
        public Condition(Compare a)
        {
            str = a+" ";
        }

        public Condition(Compare a,LinkType linkType ,Compare b)
        {
            str = a + " " + linkType + " " + b;
        }
        
        public Condition(Compare a,LinkType linkType ,Compare b,LinkType linkType2,Compare c)
        {
            str = a + " " + linkType + " " + b+ " "+linkType2+" "+c;
        }

        public override string ToString()
        {
            return str;
        }
    }

}