using System.Collections.Generic;
using DataBase.Model;

namespace DataBase.Order
{
    public class SelectOrder<T>:BaseOrder
    {
        public List<T> result;

        private Condition _condition;
        internal SelectOrder()
        {
        }
        
        public SelectOrder<T> Bind(string tableName, Condition condition=null)
        {
            _tableName = tableName;
            _condition = condition;
            return this;
        }
        
        public override bool Execute()
        {
            result = _dataBase.Select<T>(_tableName,_condition);
            return result.Count>0;
        }
    }
}