using DataBase.Model;

namespace DataBase.Order
{
    public abstract class BaseOrder
    {
        protected string _tableName;

        public string TableName
        {
            get => _tableName;
            protected set => _tableName = value;
        }

        internal BaseDataBase _dataBase
        {
            get
            {
                if (DataBaseConfig.dataBaseType == DataBaseType.MySQL)
                {
                    return MySqlDataBase.Ins;
                }
                return MongoDataBase.Ins;
            }   
        }

        public abstract bool Execute();
    }
}