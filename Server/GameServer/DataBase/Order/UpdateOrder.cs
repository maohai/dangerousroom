using DataBase.Model;

namespace DataBase.Order
{
    public class UpdateOrder : BaseOrder
    {
        private IDataModel _model;

        internal UpdateOrder()
        {
        }
        
        public UpdateOrder Bind(string tableName, IDataModel model)
        {
            _tableName = tableName;
            _model = model;

            return this;
        }
        
        public override bool Execute()
        {
            return _dataBase.Update(_tableName, _model);
        }
    }
}