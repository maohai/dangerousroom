using System;
using System.Collections.Generic;
using DataBase.Model;

namespace DataBase.Order
{
    public class DeleteOrder:BaseOrder
    {
        private Condition _condition;
        internal DeleteOrder()
        {
        }

        public DeleteOrder Bind(string tableName, Condition condition)
        {
            _tableName = tableName;
            _condition = condition;
            return this;
        }
        
        public override bool Execute()
        {
            return _dataBase.Delete(_tableName,_condition);
        }
    }
}