using System;
using System.Collections.Generic;
using System.Reflection;
using DataBase.Model;
using DataBase.Order;

namespace DataBase
{

    public enum DataBaseType
    {
        MySQL,
        MongoDB
    }
    
    internal abstract class BaseDataBase
    {

        /// <summary>
        /// 绑定数据表
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, Type> BindDataBase()
        {
            return DataBaseConfig.GetTableData?.Invoke();
        }

        public void Init()
        {
            var datas = BindDataBase();
            foreach (var keyValuePair in datas)
            {
                CreateTable(keyValuePair.Key,keyValuePair.Value);  
            }
        }
        
        /// <summary>
        /// 连接数据库
        /// </summary>
        /// <param name="databaseName">数据库名字</param>
        public abstract void Connect(string databaseName);
        
        /// <summary>
        /// 创建数据表
        /// </summary>
        /// <param name="name"></param>
        /// <param name="modelType"></param>
        public abstract void CreateTable(string tableName,Type modelType);

        /// <summary>
        /// 插入数据
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public abstract bool Insert(string tableName,IDataModel model);
        
        /// <summary>
        /// 删除表
        /// </summary>
        /// <param name="name"></param>
        public abstract void DropTable(string name);

        /// <summary>
        /// 查找数据
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="condition"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public abstract List<T> Select<T>(string tableName,Condition condition=null);

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public abstract bool Update(string tableName, IDataModel model);

        public abstract bool Delete(string tableName,Condition condition=null);

        /// <summary>
        /// 关闭数据库连接
        /// </summary>
        public abstract void Close();


        /// <summary>
        /// 通过反射获取字段属性
        /// </summary>
        /// <param name="modelType"></param>
        /// <returns></returns>
        public Dictionary<string, DataInfo> GetDataInfosByType(Type modelType)
        {
            Dictionary<string, DataInfo> dataInfos = new Dictionary<string, DataInfo>();
            
            var fields = modelType.GetFields();
            List<string> values = new List<string>();
            foreach (var fieldInfo in fields)
            {
                var info = fieldInfo.GetCustomAttribute<DataInfo>();
                if (info != null)
                {
                    dataInfos.Add(info.DataName,info);
                }
            }
            return dataInfos;
        }
    }
}