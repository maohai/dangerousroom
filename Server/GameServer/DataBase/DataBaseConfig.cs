using System;
using System.Collections.Generic;

namespace DataBase
{
    internal static class DataBaseConfig
    {
        /// <summary>
        /// 数据库类型
        /// </summary>
        public static DataBaseType dataBaseType;
        
        /// <summary>
        /// 主机
        /// </summary>
        public static string datasource="localhost";
        /// <summary>
        /// 端口
        /// </summary>
        public static int port=3306;
        /// <summary>
        /// 登录用户
        /// </summary>
        public static string user="root";
        /// <summary>
        /// 登录密码
        /// </summary>
        public static string pwd="qwer1234";
        
        /// <summary>
        /// 数据库名
        /// </summary>
        public static string dataBaseName = "";

        /// <summary>
        /// 表数据
        /// </summary>
        public static Func<Dictionary<string,Type>> GetTableData;

        /// <summary>
        /// 是否连接过
        /// </summary>
        public static bool IsConnected = false;
    }
    
    
}