using System;
using System.Collections.Generic;
using System.Reflection;
using DataBase.Model;
using DataBase.Order;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DataBase
{
    internal class MongoDataBase: BaseDataBase
    {
        private MongoClient _client;

        private IMongoDatabase _database;
        
        
        private static MongoDataBase _ins;
        private MongoDataBase()
        {
        }
        public static MongoDataBase Ins
        {
            get
            {
                if (_ins == null)
                {
                    _ins = new MongoDataBase();
                }
                return _ins;
            }
        }
        
        public override void Connect(string databaseName)
        {
            _client = new MongoClient("mongodb://localhost:27017");

            _database = _client.GetDatabase(databaseName);

            Console.WriteLine($"数据库{databaseName}连接成功");
        }

        public override void CreateTable(string tableName, Type modelType)
        {
            _database.CreateCollection(tableName);
        }

        public override bool Insert(string tableName, IDataModel model)
        {
            var document = new BsonDocument();
            var modelType = model.GetType();
            Dictionary<string, DataInfo> dataInfos = GetDataInfosByType(modelType);
            var fields= model.GetType().GetFields();
            
            foreach (var fieldInfo in fields)
            {
                var info = fieldInfo.GetCustomAttribute<DataInfo>();
                if (info != null)
                {
                    Type dataType = fieldInfo.FieldType;
                    if (dataType == typeof(int))
                    {
                        document[info.DataName] =(int) fieldInfo.GetValue(model);
                    }
                    else if (dataType == typeof(string))
                    {
                        document[info.DataName] =(string) fieldInfo.GetValue(model);
                    }
                }
            }
            var collection = _database.GetCollection<BsonDocument>(tableName);
            collection.InsertOne(document);
            
            return true;
        }

        public override void DropTable(string name)
        {
            _database.DropCollection(name);
        }

        public override List<T> Select<T>(string tableName, Condition condition=null)
        {
            var collection = _database.GetCollection<BsonDocument>(tableName);
            // var filter = Builders<BsonDocument>.Filter.Eq("Account", "123") ;

            var data = collection.Find(new BsonDocument()).ToList();
            List<T> result = new List<T>();
            Dictionary<string, FieldInfo> fieldInfos = new Dictionary<string, FieldInfo>();
            var fields = typeof(T).GetFields();
            foreach (var fieldInfo in fields)
            {
                var dataInfo = fieldInfo.GetCustomAttribute<DataInfo>();
                if (dataInfo != null)
                {
                    fieldInfos.Add(dataInfo.DataName,fieldInfo);
                }
            }
            data.ForEach(document =>
            {
                T t = Activator.CreateInstance<T>();
                foreach (var documentElement in document.Elements)
                {
                    if (fieldInfos.ContainsKey(documentElement.Name))
                    {
                        var fieldInfo = fieldInfos[documentElement.Name];
                        if (typeof(int) == fieldInfo.FieldType)
                        {
                            fieldInfo.SetValue(t,(int) documentElement.Value);
                        }
                        else
                        {
                            fieldInfo.SetValue(t,(string) documentElement.Value);
                        } 
                    }
                }
                result.Add(t);
            });
            return result;
        }

        public override bool Update(string tableName, IDataModel model)
        {
            var collection = _database.GetCollection<BsonDocument>(tableName);
            // collection.UpdateOne()

            return false;
        }

        public override bool Delete(string tableName, Condition condition = null)
        {
            throw new NotImplementedException();
        }

        public override void Close()
        {
            
        }
        
    }
}