
namespace GameClient.NetWork
{
    public class MessageEncoding
    {
        public static byte[] Encode(SocketModel value)
        {
            SocketModel model=value;
            ByteArray ba = new ByteArray();
            ba.Write(model.ipSelf);
            ba.Write(model.ipTarget);
            ba.Write(model.id);
            ba.Write((int)model.type);
            ba.Write(model.area);
            ba.Write(model.command);
            if (model.message != null)
            {
                ba.Write(SerializeObject.Encode(model.message));
            }
            byte[] result = ba.GetBuffer();
            ba.Close();
            return result;
        }

        public static SocketModel Decode(byte[] value)
        {
            ByteArray ba = new ByteArray(value);
            SocketModel model = new SocketModel();
            long id;
            int type;
            int area;
            int command;
            string ipSelf;
            string ipTarget;
            ba.Read(out ipSelf);
            ba.Read(out ipTarget);
            ba.Read(out id);
            ba.Read(out type);
            ba.Read(out area);
            ba.Read(out command);

            model.ipSelf = ipSelf;
            model.ipTarget = ipTarget;
            model.id = id;
            model.type = (PropType)type;
            model.area = area;
            model.command = command;
            if (ba.Readenable)
            {
                byte[] message;
                ba.Read(out message,ba.Length-ba.Position);
                model.message =(MsgDto)SerializeObject.Decode(message);
            }
            ba.Close();
            return model;
        }
    }
}