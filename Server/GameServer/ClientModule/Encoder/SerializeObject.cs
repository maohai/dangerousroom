using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameClient.NetWork
{
    public class SerializeObject
    {
        public static byte[] Encode(object value)
        {
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bw = new BinaryFormatter();
            bw.Serialize(ms,value);
            byte[] result = new byte[ms.Length];
            Buffer.BlockCopy(ms.GetBuffer(),0,result,0,(int)ms.Length);
            ms.Close();
            return result;
        }

        public static object Decode(byte[] buff)
        {
            MemoryStream ms = new MemoryStream(buff);
            BinaryFormatter bw = new BinaryFormatter();
            object result = bw.Deserialize(ms);
            ms.Close();
            return result;
        }
    }
}