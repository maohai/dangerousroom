namespace GameClient.NetWork
{
    /// <summary>
    /// 协议类型（客户端不能为0）
    /// </summary>
    public enum PropType
    {
        /// <summary>
        /// 连接服务
        /// </summary>
        CONNECT=1,
        
        /// <summary>
        /// 关闭
        /// </summary>
        CLOSE=2,
        
        /// <summary>
        /// 登录服务
        /// </summary>
        LOGIN,
        
        /// <summary>
        /// 游戏服务
        /// </summary>
        GAME,
        
        /// <summary>
        /// 聊天服务
        /// </summary>
        CHAT,
        
        /// <summary>
        /// 消息
        /// </summary>
        Message=100,
    }

    public class CommonProp
    {
        public const int PUSH = 1;
    }
    
    /// <summary>
    /// 登录协议
    /// </summary>
    public class LoginProp
    {
        /// <summary>
        /// 客户端申请登录
        /// </summary>
        public const int LOGIN_CREQ = 0;
        
        public const int LOGIN_SRES = 1;
        
        /// <summary>
        /// 注册请求
        /// </summary>
        public const int REGISTER_CREQ = 2;
        
        public const int REGISTER_SRES = 3;
    }

    public class GameProp
    {
        /// <summary>
        /// 上传位置
        /// </summary>
        public const int TRANSFORM_CREQ = 0;

        
        public const int TRANSFORM_SERS = 1;

    }

    public class ChatProp
    {
        /// <summary>
        /// 消息协议
        /// </summary>
        public const int MESSAGE_CREQ = 0;
        public const int MESSAGE_SERS = 1;
    }
}