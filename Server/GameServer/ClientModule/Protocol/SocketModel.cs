using System.Diagnostics;
using GameClient.NetWork;

namespace GameClient.NetWork
{
    public class SocketModel
    {
        /// <summary>
        /// 用于区分哪个服务
        /// </summary>
        public int area { get; set; }
        
        public long id { get; set; }
        /// <summary>
        /// 一级协议 用于区分模块
        /// </summary>
        public PropType type { get; set; }
        
        /// <summary>
        /// 二级协议 用于区分当前逻辑功能
        /// </summary>
        public int command { get; set; }
        
        /// <summary>
        /// 自己ip
        /// </summary>
        public string ipSelf { get; set; }
        
        /// <summary>
        /// 目标ip
        /// </summary>
        public string ipTarget { get; set; }
        
        /// <summary>
        /// 消息体 当前需要处理的对象
        /// </summary>
        public MsgDto message { get; set; }

        public SocketModel()
        {
            
        }

        public SocketModel(long i, PropType t, int a, int c, MsgDto o)
        {
            id = i;
            type = t;
            area = a;
            command = c;
            message = o;
        }
        
        public override string ToString()
        {
            string msg = "";
            if (message != null)
            {
                msg = message.ToString();
            }
            return $"ipSelf:{ipSelf},ipTarget:{ipTarget},id:{id},type:{type},area:{area},command:{command},message:[{msg}]";
        }
    }
}