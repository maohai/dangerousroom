﻿using System;
using System.Collections.Generic;
using GameClient.NetWork;
using DataBase;
using DataBase.Order;
using Server.Cache;
using Server.Model;

namespace Server
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            InitDataBase();
            new BaseNetServer.Server(100,1)
            {
                center = new HandlerCenter()
            }.Start("127.0.0.1",6666);
            while (true)
            {
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.Q)
                {
                    break;
                }
            }
        }

        private static void InitDataBase()
        {
            //连接数据库 并初始化数据库
            ConnectOrder order = OrderFactory.GetOrder<ConnectOrder>();
            order.dataBaseType = DataBaseType.MySQL;
            order.datasource = "localhost";
            order.port = 3306;
            order.user = "root";
            order.pwd = "qwer1234";
            order.dataBaseName = "Base";
            order.tablsData = () => new Dictionary<string, Type>()
            {
                {"account", typeof(AccountModel)}
            };
            order.Execute();
        }
    }
}