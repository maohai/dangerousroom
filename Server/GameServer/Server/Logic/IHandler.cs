using BaseNetServer;
using GameClient.NetWork;

namespace Server
{
    public interface IHandler
    {
        void ClientConnect(UserToken token);
        void ClientClose(UserToken token, string error);
        void MessageReceive(UserToken token, SocketModel message);
    }
}