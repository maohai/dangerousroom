using System.Collections.Generic;
using System.Net.NetworkInformation;
using BaseNetServer;
using GameClient.NetWork;
using DataBase.Order;
using Server.Model;

namespace Server.Cache
{
    public class AccountCache
    {
        private Dictionary<UserToken, string> onlineAccMap = new Dictionary<UserToken, string>();
        private Dictionary<string, UserToken> onlineAccMap1 = new Dictionary<string, UserToken>();
        /// <summary>
        /// 账号与自身属性绑定映射
        /// </summary>
        private Dictionary<string, AccountModel> accmap = new Dictionary<string, AccountModel>();

        public AccountCache()
        {
            var selectOrder = OrderFactory.GetSelectOrder<AccountModel>();
            selectOrder.Bind("account").Execute();
            foreach (var accountModel in selectOrder.result)
            {
                accmap.Add(accountModel.account,accountModel);
            }
        }
        

        #region 用户操作

        /// <summary>
        /// 用户上线
        /// </summary>
        /// <param name="token"></param>
        /// <param name="account"></param>
        public void Online(UserToken token,string account)
        {
            if (IsOnline(account))
            {
                return;
            }
            onlineAccMap.Add(token,account);
            onlineAccMap1.Add(account,token);
            Debug.Log("当前用户数量："+onlineAccMap.Count);
        }

        /// <summary>
        /// 下线
        /// </summary>
        /// <param name="token"></param>
        public void Offline(UserToken token)
        {
            if (onlineAccMap.ContainsKey(token))
            {
                Debug.Log(onlineAccMap[token]+"下线了");
                string id = onlineAccMap[token];
                onlineAccMap.Remove(token);
                onlineAccMap1.Remove(id);
                
                Debug.Log("当前用户数量："+onlineAccMap.Count);
            }
        }
        
        /// <summary>
        /// 注册用户
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public bool Create(RegisterInfoDto info)
        {
            bool isHave = HasAccount(info.account);
            if (isHave)
            {
                return false;
            }
            AccountModel model = new AccountModel();
            model.account = info.account;
            model.password = info.password;

            var insertOrder = OrderFactory.GetOrder<InsertOrder>();
            bool result = insertOrder.Bind("account", model).Execute();
            if (result)
            {
                accmap.Add(info.account,model);
            }
            return result;
        }
        
        /// <summary>
        /// 注销用户
        /// </summary>
        /// <param name="token"></param>
        /// <param name="account"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool Delete(string account)
        {
            if (HasAccount(account))
            {
                var deleteOrder = OrderFactory.GetOrder<DeleteOrder>();
                bool result = deleteOrder.Bind("account",
                    new Condition(new Compare("Account", $"'{account}'", CompareType.Equal))).Execute();
                if (result)
                {
                    UserToken token = GetUserTokenById(account);
                    if (token != null)
                    {
                        Offline(token);
                    }
                    accmap.Remove(account);
                }
                return result;
            }
            return false;
        }

        #endregion


        /// <summary>
        /// 是否有该用户
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public bool HasAccount(string account)
        {
            return accmap.ContainsKey(account);
        }

        /// <summary>
        /// 用户是否在线
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public bool IsOnline(string account)
        {
            return onlineAccMap.ContainsValue(account);
        }

        /// <summary>
        /// 检测账号密码
        /// </summary>
        /// <param name="account"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool Verify(string account, string password)
        {
            if (!HasAccount(account))
            {
                return false;
            }
            return accmap[account].password.Equals(password);
        }
        
        /// <summary>
        /// 获取用户token
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserToken GetUserTokenById(string id)
        {
            UserToken token=null;
            if (onlineAccMap1.ContainsKey(id))
            {
                token = onlineAccMap1[id];
            }
            return token;
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public AccountModel GetUserInfo(string id)
        {
            AccountModel model=null;
            if (accmap.ContainsKey(id))
            {
                model = accmap[id];
            }
            return model;
        }

        /// <summary>
        /// 获取在线用户信息
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public AccountModel GetOnlineUserInfo(UserToken token)
        {
            if (onlineAccMap.ContainsKey(token))
            {
                return GetUserInfo(onlineAccMap[token]);
            }
            return null;
        }

        public List<UserToken> GetOnlineUser()
        {
            List<UserToken> userTokens = new List<UserToken>();
            foreach (var keyValuePair in onlineAccMap)
            {
                userTokens.Add(keyValuePair.Key);
            }

            return userTokens;
        }
    }
}