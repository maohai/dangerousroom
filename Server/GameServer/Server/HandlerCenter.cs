using System.Collections.Generic;
using BaseNetServer;
using GameClient.NetWork;
using Server.Cache;

namespace Server
{
    public class HandlerCenter:AdsHandlerCenter
    {
        private List<BaseServer> allServer;

        public HandlerCenter()
        {
            allServer = new List<BaseServer>();
            allServer.Add(new LoginServer());
        }


        public override void ConnectSuccess(string ipSelf)
        {
            BaseServer.ipSelf = ipSelf;
        }

        public override void ClientConnect(UserToken token)
        {
            allServer.ForEach(server =>
            {
                server.ClientConnect(token);
            });
        }

        public override void MessageReceive(UserToken token, SocketModel message)
        {
            if (message == null)
            {
                return;
            }
            allServer.ForEach(server =>
            {
                if (server.GetPropType() == message.type)
                {
                    server.MessageReceive(token,message);
                }
            });
        }

        public override void ClientClose(UserToken token, string error)
        {
            allServer.ForEach(server =>
            {
                server.ClientClose(token,error);
            });
        }
    }
}