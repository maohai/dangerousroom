using System;
using System.Collections.Generic;
using System.Linq;
using GameClient.Data;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameClient
{
    public enum Language
    {
        Zh,//中文
        En,//英文
    }
    public class LocalizationModule : IMod
    {
        public Language CurLanguage;
        
        private Dictionary<string, string> _datas;
        
        public void Init(JObject db, JObject saveData, Action cb)
        {
            CurLanguage = Language.Zh;
            _datas = new Dictionary<string, string>();
            var data = db["Localization"].ToList();
            foreach (var jToken in data)
            {
                if (!_datas.TryAdd(jToken["Key"].ToString(), jToken[CurLanguage.ToString()].ToString()))
                {
                    Debug.LogError(jToken["Key"].ToString()+"重复了");
                }
            }

            Debug.Log(Lstring.TxtHello.T());
            cb?.Invoke();
        }

        public JObject GetSaveData()
        {
            return null;
        }

        public string GetValue(string key)
        {
            if (_datas.ContainsKey(key))
            {
                return _datas[key];
            }
            return key;
        }
    }

}