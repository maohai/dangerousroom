using UnityEngine;

namespace GameClient
{
    public static class LocalizationExpand
    {
        public static string T(this string str, params object[] param)
        {
            string value = Client.Ins.Localization.GetValue(str);
            if (string.IsNullOrEmpty(value))
            {
                Debug.LogError(str+" 本地化不存在");
                return str;
            }
            if (param.Length > 0)
            {
                return string.Format(value, param);
            }
            
            return value;
        }
    }

}