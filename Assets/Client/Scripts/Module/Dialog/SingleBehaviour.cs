using UnityEngine;

namespace GameClient
{
    public class SingleBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _ins;

        public static T Ins
        {
            get
            {
                return _ins;
            }
        }

        protected virtual void Awake()
        {
            _ins = this as T;
        }

        protected virtual void OnDestroy()
        {
            _ins = null;
        }


    }
}
