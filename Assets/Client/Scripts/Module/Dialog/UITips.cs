using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace GameClient
{
    public class UITips : SingleBehaviour<UITips>
    {
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private Text Content;

        private Sequence se;

        public static void Show(string text)
        {
            Ins.OnShow(text);
        }

        protected override void Awake()
        {
            base.Awake();
            gameObject.SetActive(false);
        }

        /// <summary>
        /// 隐藏tip
        /// </summary>
        public static void Hide()
        {
            if (Ins != null)
            {
                Ins.OnHide();
            }
        }

        private void OnShow(string text)
        {
            gameObject.SetActive(true);
            Content.text =  text;
            canvasGroup.alpha = 0;
            if (se != null)
            {
                se.Kill();
            }
            float duration = 2;
            se = DOTween.Sequence();
            se.Append(canvasGroup.DOFade(1, duration * 0.1f)).AppendInterval(duration * 0.6f)
                .Append(canvasGroup.DOFade(0, duration * 0.3f))
                .AppendCallback(
                    () => gameObject.SetActive(false)
                    );
        }

        private void OnHide()
        {
            se.Kill();
            gameObject.SetActive(false);
        }
    }
}