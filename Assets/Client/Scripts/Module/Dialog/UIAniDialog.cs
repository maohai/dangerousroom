using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace GameClient
{
    public abstract class UIAniDialog : UIDialog
    {
        protected override void Awake()
        {
            base.Awake();
            tr.localScale=Vector3.zero;
            ClickSwitch = false;
            tr.DOKill();
            tr.DOScale(Vector3.one, 0.3f).SetEase(Ease.OutBack).OnComplete(() =>
            {
                ClickSwitch = true;
                OnUIShowCompile();
            });
        }

        protected override void OnUIShow(params object[] param)
        {
            
        }

        public void Close(Action cb=null)
        {
            ClickSwitch = false;
            tr.DOKill();
            tr.DOScale(Vector3.zero, 0.2f).SetEase(Ease.InBack).OnComplete(() =>
            {
                dialog.Close(this);
                cb?.Invoke();
            });
        }

        protected virtual void OnUIShowCompile()
        {
            
        }
    }

}