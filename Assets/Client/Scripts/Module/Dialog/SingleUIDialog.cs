using UnityEngine;

namespace GameClient
{
    public class SingleUIDialog<T> : UIDialog where T : MonoBehaviour
    {
        private static T _ins;

        public static T Ins
        {
            get
            {
                return _ins;
            }
        }

        protected override void Awake()
        {
            _ins = this as T;
            base.Awake();
        }

        protected virtual void OnDestroy()
        {
            _ins = null;
        }

    }
}
