using UnityEngine;

namespace GameClient
{
    public abstract class UIDialog : MonoBehaviour
    {
        [HideInInspector]
        public Transform tr;

        private int sortingOrder;
        private GameObject goClickMask;


        /// <summary>
        /// 是否可以点击
        /// </summary>
        public bool ClickSwitch
        {
            get => goClickMask.activeSelf;
            set
            {
                goClickMask.SetActive(!value);
            }
        }
        public int SortingOrder => sortingOrder;
        protected static DialogModule dialog => Client.Ins.Dialog;
        protected virtual void Awake()
        {
            tr = transform;
            var p = tr.parent;
            sortingOrder = p.GetComponent<Canvas>().sortingOrder;
            goClickMask = p.GetChild(0).gameObject;
            ClickSwitch = true;
        }

        protected virtual void OnUIShow(params object[] param)
        {
            
        }

        protected virtual void OnUIClose()
        {

        }

        protected virtual void OnUIResume()
        {
            
        }

        public virtual void Close()
        {
            Client.Ins.Dialog.Back();
        }

        public void ShowTips(string msg)
        {
            UITips.Show(msg);
        }

    }
}
