using System.Collections;
using System.Collections.Generic;
using GameClient.Data;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Game{
    public class CharacterInfo{
        public int ID;
        public CharacterData Data;
        public int Level;
        
        public int MaxHP;
        public int MaxMP;

        public CharacterInfo(CharacterData data){
            this.Data = data;
            ID = data.ID;
            MaxHP = data.HP;
            MaxMP = data.MP;
        }
    }

}