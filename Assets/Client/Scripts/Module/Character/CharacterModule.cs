using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Game;
using GameClient.Data;
using CharacterInfo = Game.CharacterInfo;

namespace GameClient{
    public class CharacterModule : IMod{

        public static readonly ClientEvent OnEnemyHP = new ClientEvent();
        
        public List<CharacterData> CharacterDataList = new List<CharacterData>();
        public List<CharacterInfo> CharacterInfoList = new List<CharacterInfo>();
        
        public void Init(JObject db, JObject json, Action cb){
            var dataList = db["Character"].ToList();
            foreach (var item in dataList){
                // Debug.Log(item["Name"]);
                var data = new CharacterData(item);
                var info = new CharacterInfo(data);
                CharacterDataList.Add(data);
                CharacterInfoList.Add(info);
            }
            cb?.Invoke();
        }

        public JObject GetSaveData(){
            return null;
        }

        public CharacterInfo GetInfoByID(int id){
            try{
                return CharacterInfoList.Find(a => a.ID == id);
            }
            catch (Exception e){
                Console.WriteLine("没有这个信息ID");
                throw;
            }
        }
    }
}
