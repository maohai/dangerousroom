using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Newtonsoft.Json.Linq;
using UnityEngine;
using System.Security.Cryptography;
using Base;
using Cysharp.Threading.Tasks;
using GameClient.NetWork;
using YooAsset;

namespace GameClient
{
    public class Client : SingleBehaviour<Client>
    {
        public static async UniTask Run()
        {
            Debug.Log("加载UI");
            var package =await YooAssetManager.LoadPackage("Main");
#if !UNITY_EDITOR
            var  infos = package.GetAssetInfos("HotDll");
            foreach (var assetInfo in infos)
            {
                if (Path.GetFileName(assetInfo.AssetPath) != "ClientModule.dll.bytes")
                {
                    var a = package.LoadRawFileAsync(assetInfo);
                    await a;
                    Assembly.Load(a.GetRawFileData());
                }
            }
#endif
            var aoh = package.LoadAssetAsync<GameObject>("UICanvas");
            await aoh;
            aoh.InstantiateSync();
        }
        
        private ClientConfig _config;

        /// <summary>
        /// 配置信息
        /// </summary>
        public ClientConfig Config => _config;

        #region 模块
        /// <summary>
        /// 玩家用户
        /// </summary>
        public readonly PlayerModule Player=new PlayerModule();
        /// <summary>
        /// 本地化
        /// </summary>
        public readonly LocalizationModule Localization=new LocalizationModule();
        /// <summary>
        /// 弹窗
        /// </summary>
        public DialogModule Dialog;
        /// <summary>
        /// 网络
        /// </summary>
        [HideInInspector]
        public NetWorkModule NetWork;

        public CharacterModule Character = new CharacterModule();
        /// <summary>
        /// 事件
        /// </summary>
        public EventModule Event=new EventModule();
        #endregion

        /// <summary>
        /// 所有模块加载完成事件
        /// </summary>
        public event Action OnLoadAllModulesFinish ;
        
        #region 私有变量
        private readonly string SAVE_KEY = "QWERABCD";
        private int curLoadIndex = 0;//当前加载第几个模块
        private List<IMod> _mods = new List<IMod>();
        private bool isLoadComplete;
        #endregion

        #region 初始化
        private void Start()
        {
            _config = Resources.Load<ClientConfig>("ClientConfig");
            InitModules();
            InitLoad();
            DontDestroyOnLoad(gameObject);
        }

        /// <summary>
        /// 初始化模块
        /// </summary>
        private void InitModules()
        {
            _mods.Add(Event);
            _mods.Add(Localization);
            _mods.Add(Dialog);
            NetWork = gameObject.AddComponent<NetWorkModule>();
            _mods.Add(NetWork);
            _mods.Add(Player);
            _mods.Add(Character);
        }

        private async void InitLoad()
        {
            //加载持久化数据
            JObject saveData = new JObject();
            var savePath = GetSavePath();
            if (File.Exists(savePath))
            {
                FileStream fs = new FileStream(savePath, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                long start = br.BaseStream.Position;
                //获取数据长度
                br.BaseStream.Seek(0, SeekOrigin.End);
                long end = br.BaseStream.Position;
                int byteCount = (int) (end - start);
                br.BaseStream.Seek(0, SeekOrigin.Begin);
                byte[] b = br.ReadBytes(byteCount);
                br.Close();
                fs.Close();
                try
                {
                    if (Config.SaveDataEncryption)
                    {
                        b = AESDecrypt(b, SAVE_KEY);
                    }
                    string data = Encoding.UTF8.GetString(b);
                    saveData = JObject.Parse(data);
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                    saveData = new JObject();
                }
            }

            var package = await YooAssetManager.LoadPackage("Main");
            var d = package.LoadRawFileAsync("Data");
            await d;
            JObject jo = JObject.Parse(d.GetRawFileText());
            jo = CustomParse(jo);
            //加载所有模块
            StartCoroutine(LoadModules(jo, saveData));
        }

        //递归初始化模块
        private IEnumerator LoadModules(JObject jo, JObject saveData)
        {
            foreach (var mod in _mods)
            {
                StartCoroutine(LoadSingleModule(mod, jo, saveData));
            }
            yield return new WaitUntil(() => curLoadIndex >= _mods.Count);
            OnInitModuleCompile();
        }

        private IEnumerator LoadSingleModule(IMod mod,JObject db,JObject saveData)
        {
            bool isFinish = false;
            var modName = mod.GetType().Name;
            //保存的模块数据
            JObject d = (JObject) saveData[modName];
            if (d == null)
            {
                d = new JObject();
            }
            //开始时间
            long startTimestamp = Utility.GetCurTimestampMilliseconds();
            mod.Init(db,d, () =>
            {
                isFinish = true;
                //结束时间
                long endTimestamp = Utility.GetCurTimestampMilliseconds();
                Debug.Log($"加载模块{modName} 耗时{endTimestamp-startTimestamp}ms");
            });
            yield return new WaitUntil(() => isFinish);
            curLoadIndex++;
        }

        /// <summary>
        /// 所有模块加载完成触发
        /// </summary>
        private void OnInitModuleCompile()
        {
            Debug.Log("所有模块初始化完成");
            isLoadComplete = true;
            OnLoadAllModulesFinish?.Invoke();
        }
        

        #endregion

        #region 解析、保存数据
        private void SaveData()
        {
            var data = new JObject();
            for (int i = 0; i < _mods.Count; i++)
            {
                var m = _mods[i];
                var d = m.GetSaveData();
                if (d != null)
                {
                    data[m.GetType().Name] = d;
                }
            }

            string saveDataPath = GetSavePath();
            Debug.Log("保存路径" + saveDataPath);
            FileStream stream = new FileStream(saveDataPath, FileMode.Create);
            byte[] bytes = Encoding.UTF8.GetBytes(data.ToString());
            if (Config.SaveDataEncryption)
            {
                bytes  = AESEncrypt(bytes, SAVE_KEY);
            }
            stream.Write(bytes, 0, bytes.Length);
            stream.Close();
        }
        
        private JObject CustomParse(JObject jo)
        {
            JObject newJo = new JObject();
            foreach (var keyValuePair in jo)
            {
                JArray data = keyValuePair.Value.ToObject<JArray>();

                string[] names = data[0].ToObject<string[]>(); //属性名
                JArray j = new JArray();
                for (int y = 1; y < data.Count; y++)
                {
                    JObject d = new JObject();
                    for (int x = 0; x < names.Length; x++)
                    {
                        d[names[x]] = data[y].ToObject<JArray>()[x];
                    }

                    j.Add(d);
                }

                newJo[keyValuePair.Key] = j;
            }

            return newJo;
        }

        private string GetSavePath()
        {
            return Path.Combine(Application.persistentDataPath, "SaveData");
        }

        /// <summary>
        /// AES 加密(高级加密标准，是下一代的加密算法标准，速度快，安全级别高，目前 AES 标准的一个实现是 Rijndael 算法)
        /// </summary>
        /// <param name="EncryptString">待加密密文</param>
        /// <param name="EncryptKey">加密密钥</param>
        private byte[] AESEncrypt(byte[] EncryptByte, string EncryptKey)
        {
            if (EncryptByte.Length == 0)
            {
                throw (new Exception("明文不得为空"));
            }

            if (string.IsNullOrEmpty(EncryptKey))
            {
                throw (new Exception("密钥不得为空"));
            }

            byte[] m_strEncrypt;
            byte[] m_btIV = Convert.FromBase64String("Rkb4jvUy/ye7Cd7k89QQgQ==");
            byte[] m_salt = Convert.FromBase64String("gsf4jvkyhye5/d7k8OrLgM==");
            Rijndael m_AESProvider = Rijndael.Create();
            try
            {
                MemoryStream m_stream = new MemoryStream();
                PasswordDeriveBytes pdb = new PasswordDeriveBytes(EncryptKey, m_salt);
                ICryptoTransform transform = m_AESProvider.CreateEncryptor(pdb.GetBytes(32), m_btIV);
                CryptoStream m_csstream = new CryptoStream(m_stream, transform, CryptoStreamMode.Write);
                m_csstream.Write(EncryptByte, 0, EncryptByte.Length);
                m_csstream.FlushFinalBlock();
                m_strEncrypt = m_stream.ToArray();
                m_stream.Close();
                m_stream.Dispose();
                m_csstream.Close();
                m_csstream.Dispose();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            catch (CryptographicException ex)
            {
                throw ex;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                m_AESProvider.Clear();
            }

            return m_strEncrypt;
        }

        // 解密字节数组
        /// <summary>
        /// AES 解密(高级加密标准，是下一代的加密算法标准，速度快，安全级别高，目前 AES 标准的一个实现是 Rijndael 算法)
        /// </summary>
        /// <param name="DecryptString">待解密密文</param>
        /// <param name="DecryptKey">解密密钥</param>
        private byte[] AESDecrypt(byte[] DecryptByte, string DecryptKey)
        {
            if (DecryptByte.Length == 0)
            {
                throw (new Exception("密文不得为空"));
            }

            if (string.IsNullOrEmpty(DecryptKey))
            {
                throw (new Exception("密钥不得为空"));
            }

            byte[] m_strDecrypt;
            byte[] m_btIV = Convert.FromBase64String("Rkb4jvUy/ye7Cd7k89QQgQ==");
            byte[] m_salt = Convert.FromBase64String("gsf4jvkyhye5/d7k8OrLgM==");
            Rijndael m_AESProvider = Rijndael.Create();
            try
            {
                MemoryStream m_stream = new MemoryStream();
                PasswordDeriveBytes pdb = new PasswordDeriveBytes(DecryptKey, m_salt);
                ICryptoTransform transform = m_AESProvider.CreateDecryptor(pdb.GetBytes(32), m_btIV);
                CryptoStream m_csstream = new CryptoStream(m_stream, transform, CryptoStreamMode.Write);
                m_csstream.Write(DecryptByte, 0, DecryptByte.Length);
                m_csstream.FlushFinalBlock();
                m_strDecrypt = m_stream.ToArray();
                m_stream.Close();
                m_stream.Dispose();
                m_csstream.Close();
                m_csstream.Dispose();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            catch (CryptographicException ex)
            {
                throw ex;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                m_AESProvider.Clear();
            }

            return m_strDecrypt;
        }

        #endregion
        
        private void OnApplicationQuit()
        {
            Debug.Log("退出");
            SaveData();
        }
        /// <summary>
        /// 获取加载进度（0-1）若为1则加载完成
        /// </summary>
        /// <returns></returns>
        public float GetLoadProgress()
        {
            if (_mods.Count == 0)
            {
                return 1;
            }

            if (isLoadComplete)
            {
                return 1;
            }
            return curLoadIndex * 1f / _mods.Count-0.01f;
        }

        /// <summary>
        /// 延时多少秒执行
        /// </summary>
        /// <param name="cb"></param>
        /// <param name="t"></param>
        public void Wait(Action cb, float t)
        {
            StartCoroutine(_Wait(cb, t));
        }

        private IEnumerator _Wait(Action cb, float t)
        {
            yield return new WaitForSeconds(t);
            cb?.Invoke();
        }
    }
}