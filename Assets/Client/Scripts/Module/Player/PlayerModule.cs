using System;
using GameClient.NetWork;
using Newtonsoft.Json.Linq;

namespace GameClient
{
    public class PlayerModule : IMod
    {
        /// <summary>
        /// 账号
        /// </summary>
        private string account;

        private string password;

        public void Init(JObject db, JObject saveData, Action cb)
        {
            // var m = db["Monster"].ToList();
            // foreach (var jToken in m)
            // {
            //     Debug.Log(jToken);
            //     var data = new MonsterData(jToken);
            // }
            InitJson(saveData);
            cb?.Invoke();
        }

        private void InitJson(JObject saveData)
        {
            if (saveData["Account"] != null)
            {
                account =saveData["Account"].ToObject<string>();
            }

            if (saveData["Password"] != null)
            {
                password = saveData["Password"].ToObject<string>();
            }
        }

        public JObject GetSaveData()
        {
            JObject data = new JObject();
            data["Account"] = account;
            data["Password"] = password;
            return data;
        }

        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="dto"></param>
        public void Register(RegisterInfoDto dto,Action cb)
        {
            this.Req(PropType.LOGIN,LoginProp.REGISTER_CREQ,new MsgDto(dto), model =>
            {
                if (model.message.success)
                {
                    account = dto.account;
                    password = dto.password;
                }
                cb?.Invoke();
            });
        }
    }
}
