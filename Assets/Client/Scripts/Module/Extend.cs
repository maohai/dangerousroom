using System;
using GameClient.NetWork;
using UnityEngine;
using JArray = Newtonsoft.Json.Linq.JArray;
using JToken = Newtonsoft.Json.Linq.JToken;

namespace GameClient
{
    public static class Extend
    {
        #region 网络拓展
        
        public static void Req(this MonoBehaviour mono,PropType type,int command,MsgDto message,Action<SocketModel> cb=null)
        {
#if !UNITY_WEBGL 
            Client.Ins.NetWork.Req(type,command,message,cb);
#endif
        }
        
        public static void Req(this IMod mono,PropType type,int command,MsgDto message,Action<SocketModel> cb=null)
        {
#if !UNITY_WEBGL 
            Client.Ins.NetWork.Req(type,command,message,cb);
#endif
        }

        #endregion


        #region Jobject 拓展

        public static T[] ToCustomArray<T>(this JToken token)
        {
            JArray array=JArray.Parse(token.ToString());
            T[] data = new T[array.Count];
            int i = 0;
            foreach (var jToken in array)
            {
                data[i] = jToken.ToObject<T>();
                i++;
            }
            return data;
        }


        #endregion
    }

}