using System;
using System.Collections.Generic;
#if !UNITY_WEBGL
using System.Net.Sockets;
#endif
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameClient.NetWork
{
    public class NetWorkModule : MonoBehaviour,IMod
    {
        public static readonly ClientEvent EventServerMessage=new ClientEvent();
        public void Init(JObject db, JObject saveData, Action cb)
        {
#if !UNITY_WEBGL
            StartServer();
#endif
            cb?.Invoke();
        }

        public JObject GetSaveData()
        {
            return null;
        }
#if !UNITY_WEBGL
#region 服务
        private byte[] readBuff = new byte[1024];
        private Socket socket;
        private List<SocketModel> messages = new List<SocketModel>();
        private bool isReading = false;
        private string id;
        private List<byte> cache = new List<byte>();

        private static long msgID;
        private Dictionary<long, Action<SocketModel>> writeCb = new Dictionary<long, Action<SocketModel>>();

        private int area = 1;
        private void StartServer()
        {
            if (!Client.Ins.Config.StarSocketServer)
            {
                return;
            }
            try
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                var ipInfo = Client.Ins.Config.ServerIpAddress.Split(":");
                string serverIp = ipInfo[0];
                int port =int.Parse(ipInfo[1]);
                Debug.Log("连接" + serverIp);
                socket.Connect(serverIp, port);
                socket.BeginReceive(readBuff, 0, 1024, SocketFlags.None, ReceiveCallBack, readBuff);
                
                //发送自身ip连接成功
                ByteArray arr = new ByteArray();
                byte[] ba = MessageEncoding.Encode(new SocketModel(){ipSelf = socket.LocalEndPoint.ToString(),ipTarget = socket.RemoteEndPoint.ToString(),area = area,id=0,type = PropType.CONNECT,message = new MsgDto(false,socket.LocalEndPoint.ToString())});
                arr.Write(ba.Length);
                arr.Write(ba);
                socket.Send(arr.GetBuffer());
                
            }
            catch (Exception e)
            {
                Debug.LogError("连接失败"+e.Message);
            }
        }
        private void Update()
        {
            while (messages.Count>0)
            {
                SocketModel model = messages[0];
                StartCoroutine("MessageReceive", model);
                messages.RemoveAt(0);
            }
        }
        private void MessageReceive(SocketModel model)
        {
            Debug.Log("Server:{"+model+"}");
            var cb = GetWriteCb(model);
            if (cb!=null)
            {
                if (model.type == PropType.Message)//服务器发送的消息
                {
                   UITips.Show(model.message.GetMsg<string>());
                }
                else
                {
                    cb.Invoke(model);
                }
            }
            else
            {
                if (model.type == PropType.LOGIN && model.command == LoginProp.LOGIN_SRES)
                {
                    Debug.Log(model.message.GetMsg<string>());
                    id = model.message.GetMsg<string>();
                    return;
                }
                EventServerMessage.Trigger(model);
            }
        }
        private void ReceiveCallBack(IAsyncResult ar)
        {
            int length = socket.EndReceive(ar);
            byte[] message = new byte[length];
            
            Buffer.BlockCopy(readBuff, 0, message, 0, length);
            cache.AddRange(message);
            if (!isReading)
            {
                isReading = true;
                OnData();
                socket.BeginReceive(readBuff, 0, 1024, SocketFlags.None, ReceiveCallBack, readBuff);
            }
        }
        private void OnData()
        {
            byte[] buff = null;
            buff = LengthEncoding.Decode(ref cache);
            if (buff == null)
            {
                isReading = false;
                return;
            }
            SocketModel message = MessageEncoding.Decode(buff);
            if (message == null)
            {
                isReading = false;
                return;
            }
            messages.Add(message);
            OnData();
        }
        private void Close()
        {
            if (socket != null)
            {
                Debug.Log("关闭socket");
                socket.Dispose();
                socket.Close();
            }
        }
        private void OnDestroy()
        {
            Close();
        }
        private void Req(SocketModel model,Action<SocketModel> cb=null)
        {
            if (socket==null||!socket.Connected)
            {
                UITips.Show("网络未连接");
                cb = null;
                StartServer();
                return;
            }

            model.area = area;
            model.ipTarget = socket.RemoteEndPoint.ToString();
            model.ipSelf = socket.LocalEndPoint.ToString();
            Debug.Log("Client:{"+model+"}");
            ByteArray arr = new ByteArray();
            byte[] ba = MessageEncoding.Encode(model);
            arr.Write(ba.Length);
            arr.Write(ba);
            try
            {
                socket.Send(arr.GetBuffer());
                PushWriteCb(model, cb);
            }
            catch (Exception e)
            {
                Debug.LogError("网络错误"+e.Message);
                cb?.Invoke(null);
            }
            arr.Close();
        }
        public void Req(PropType type, int command, MsgDto msg,Action<SocketModel> cb=null)
        {
            msgID++;
            Req(new SocketModel(msgID,type, -1, command, msg),cb);
        }
        public void Req(PropType type, int area, int command, MsgDto msg,Action<SocketModel> cb=null)
        {
            msgID++;
            Req(new SocketModel(msgID,type, area, command, msg),cb);
        }
        private void PushWriteCb(SocketModel clientModel,Action<SocketModel> cb)
        {
            if (clientModel.id <0 ||cb==null)//没有回调
            {
                return;
            }
            long k = clientModel.id;
            if (!writeCb.ContainsKey(k))
            {
                writeCb.Add(k,cb);
            }
        }
        private Action<SocketModel> GetWriteCb(SocketModel serverModel)
        {
            long k = serverModel.id;
            if (writeCb.ContainsKey(k))
            {
                var value = writeCb[k];
                writeCb.Remove(k);
                return value;
            }
            return null;
        }
#endregion

#endif
    }
}