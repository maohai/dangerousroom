using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GameClient.NetWork
{
    /// <summary>
    /// 消息协议
    /// </summary>
    [Serializable]
    public class MsgDto
    {
        public bool success;
        private string msg;

        public MsgDto(bool success, object msg)
        {
            Init(success,msg);
        }

        private void Init(bool success, object msg)
        {
            this.success = success;
            if (msg == null)
            {
                return;
            }

            Type t = msg.GetType();
            if (msg is string || t.IsPrimitive)
            {
                this.msg =msg.ToString();
            }
            else
            {
                this.msg = ((JObject)JToken.FromObject(msg)).ToString();
            }
        }

        public MsgDto(object msg)
        {
            Init(true,msg);
        }

        public T GetMsg<T>()
        {
            return JsonConvert.DeserializeObject<T>(msg.ToString());
        }

        public override string ToString()
        {
            return $"success:{success},msg:{msg}";
        }
    }
}