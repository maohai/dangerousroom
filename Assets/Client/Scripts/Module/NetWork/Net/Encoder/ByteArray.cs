using System;
using System.IO;

namespace GameClient.NetWork
{
    public class ByteArray
    {
        private MemoryStream ms = new MemoryStream();
        private BinaryWriter bw;
        private BinaryReader br;

        public void Close()
        {
            bw.Close();
            br.Close();
            ms.Close();
        }

        public ByteArray(byte[] buff)
        {
            ms = new MemoryStream(buff);
            bw = new BinaryWriter(ms);
            br = new BinaryReader(ms);
        }

        public ByteArray()
        {
            bw = new BinaryWriter(ms);
            br = new BinaryReader(ms);
        }

        public int Position
        {
            get { return (int) ms.Position; }
        }

        public int Length
        {
            get { return (int) ms.Length; }
        }

        public bool Readenable
        {
            get{
                return ms.Length > ms.Position;
            }
        }

        public byte[] GetBuffer()
        {
            byte[] result = new byte[ms.Length];
            Buffer.BlockCopy(ms.GetBuffer(),0,result,0,(int)ms.Length);
            return result;
        }

        public void Read(out int value)
        {
            value = br.ReadInt32();
        }
        public void Read(out byte value)
        {
            value =br.ReadByte();
        }
        public void Read(out bool value)
        {
            value = br.ReadBoolean();
        }
        public void Read(out string value)
        {
            value = br.ReadString();
        }
        public void Read(out byte[] value,int length)
        {
            value = br.ReadBytes(length);
        }
        public void Read(out float value)
        {
            value = br.ReadSingle();
        }
        public void Read(out long value)
        {
            value = br.ReadInt64();
        }

        
        public void Write(int value)
        {
            bw.Write(value);
        }
        public void Write(byte value)
        {
            bw.Write(value);
        }
        public void Write(bool value)
        {
            bw.Write(value);
        }
        public void Write(string value)
        {
            bw.Write(value);
        }
        public void Write(byte[] value)
        {
            bw.Write(value);
        }
        public void Write(float value)
        {
            bw.Write(value);
        }
        public void Write(long value)
        {
            bw.Write(value);
        }
        
    }
    
}