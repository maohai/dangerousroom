
using System;
using Newtonsoft.Json.Linq;

namespace GameClient
{
    public class GameModule : IMod
    {
        public void Init(JObject db, JObject saveData, Action cb)
        {
            cb?.Invoke();
        }

        public JObject GetSaveData()
        {
            return null;
        }
    }
}