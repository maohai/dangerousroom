using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameClient {
    public class EventModule : IMod {
        private Dictionary<int, Action<object[]>> evtMap =
            new Dictionary<int, Action< object[]>>();
        
        public void Register(ClientEvent evt, Action<object[]> listener) {
            if (!evtMap.ContainsKey(evt.ID)) {
                evtMap[evt.ID] = listener;
            } else {
                evtMap[evt.ID] += listener;
            }
        }

        public void Remove(ClientEvent evt, Action<object[]> listener) {
            if (evtMap.ContainsKey(evt.ID)) {
                evtMap[evt.ID] -= listener;
            }
        }
        
        public void RemoveAll(ClientEvent evt) {
            evtMap.Remove(evt.ID);
        }

        public void RemoveAll() {
            evtMap.Clear();
        }

        public void Trigger(ClientEvent evt, params object[] args) {
            evtMap.TryGetValue(evt.ID, out var cb);
            try {
                cb?.Invoke(args);
            } catch (Exception e) {
                Debug.LogException(e);
            }
        }

        public void Init(JObject db, JObject saveData, Action cb)
        {
            cb?.Invoke();
        }

        public JObject GetSaveData()
        {
            return null;
        }
    }
}