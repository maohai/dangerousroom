
using Newtonsoft.Json.Linq;

namespace GameClient.Data
{
    public class CharacterData
    {
		/// <summary>
		/// ID
		/// </summary>
		public int ID{ get; set; }
		/// <summary>
		/// 名字
		/// </summary>
		public string Name{ get; set; }
		/// <summary>
		/// 生命值
		/// </summary>
		public int HP{ get; set; }
		/// <summary>
		/// 法力值
		/// </summary>
		public int MP{ get; set; }
		/// <summary>
		/// 攻击力
		/// </summary>
		public int Attack{ get; set; }

        public CharacterData(JToken data)
        {
			ID = data["ID"].ToObject<int>();
			Name = data["Name"].ToObject<string>();
			HP = data["HP"].ToObject<int>();
			MP = data["MP"].ToObject<int>();
			Attack = data["Attack"].ToObject<int>();
        }
    }
}
