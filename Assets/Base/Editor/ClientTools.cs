using System;
using System.Diagnostics;
using System.IO;
using Base;
using HybridCLR.Editor;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Client
{
    public class ClientTools
    {
        [MenuItem("Tools/ClientTools/打开保存路径 &o")]
        static void ShowSavePath()
        {
            Process.Start(Application.persistentDataPath);
        }

        [MenuItem("Tools/ClientTools/Copy协议到客户端 &C")]
        static void MovePProtocolToClient()
        {
            string projectPath = Directory.GetParent(Application.dataPath).ToString();
            string serverPath = Path.Combine(projectPath,"Server", "GameServer", "ClientModule");
            string netPath = Path.Combine(projectPath, "Assets", "Client", "Scripts", "Module", "NetWork", "Net");
            if (Directory.Exists(netPath))
            {
                Directory.Delete(netPath, true);
            }

            CopyDirectory(Path.Combine(serverPath, "Dto"), Path.Combine(netPath, "Dto"));
            CopyDirectory(Path.Combine(serverPath, "Encoder"), Path.Combine(netPath, "Encoder"));
            CopyDirectory(Path.Combine(serverPath, "Protocol"), Path.Combine(netPath, "Protocol"));

            AssetDatabase.Refresh();
        }

        [InitializeOnLoadMethod]
        static void OnInit()
        {
            CreateClientAssets();
        }

        /// <summary>
        /// 创建配置文件
        /// </summary>
        static void CreateClientAssets()
        {
            string assetsPath = "Assets/Resources/ClientConfig.asset";
            if (File.Exists(assetsPath))
            {
                // Debug.Log("配置文件已存在");
                return;
            }

            var ass = ScriptableObject.CreateInstance<ClientConfig>();
            ass.BuildPlatform = Platform.StandaloneWindows64;
            ass.HotIpAddress = "http://127.0.0.1:8000";
            ass.ServerIpAddress = "127.0.0.1:6666";
            AssetDatabase.CreateAsset(ass, assetsPath);
            AssetDatabase.Refresh();
            Debug.Log("配置文件生成" + assetsPath);
        }

        /// <summary>
        /// 复制文件夹中的所有内容
        /// </summary>
        /// <param name="sourceDirPath">源文件夹目录</param>
        /// <param name="saveDirPath">指定文件夹目录</param>
        static void CopyDirectory(string sourceDirPath, string saveDirPath)
        {
            try
            {
                if (!Directory.Exists(saveDirPath))
                {
                    Directory.CreateDirectory(saveDirPath);
                }

                string[] files = Directory.GetFiles(sourceDirPath);
                foreach (string file in files)
                {
                    string pFilePath = saveDirPath + "\\" + Path.GetFileName(file);
                    if (File.Exists(pFilePath))
                        continue;
                    File.Copy(file, pFilePath, true);
                }

                string[] dirs = Directory.GetDirectories(sourceDirPath);
                foreach (string dir in dirs)
                {
                    CopyDirectory(dir, saveDirPath + "\\" + Path.GetFileName(dir));
                }

                Debug.Log("copy success");
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
            }
        }
    }

    public class HotTool
    {
        [MenuItem("HybridCLR/拷贝到Assets")]
        static void CopyToAssets()
        {
            var buildTarget = EditorUserBuildSettings.activeBuildTarget.ToString();
            string hotSrc = Path.Combine("HybridCLRData", "HotUpdateDlls", buildTarget);
            string aotSrc = Path.Combine("HybridCLRData", "AssembliesPostIl2CppStrip", buildTarget);
            if (Directory.Exists(hotSrc)&&Directory.Exists(aotSrc))
            {
                //更新 热更dll
                string hot = Path.Combine(Application.dataPath, "Hot",buildTarget,"HotDll");
                if (Directory.Exists(hot))
                {
                    Directory.Delete(hot,true);
                }
                Directory.CreateDirectory(hot);
                foreach (var s in SettingsUtil.HotUpdateAssemblyFilesExcludePreserved)
                {
                    Debug.Log($"copy {Path.Combine(hotSrc,s)} to {Path.Combine(hot,s+".bytes")}");
                    File.Copy(Path.Combine(hotSrc,s),Path.Combine(hot,s+".bytes"),true);
                }
                //更新 AOT
                string aot = Path.Combine(Application.dataPath, "Hot", buildTarget, "AotDll");
                if (Directory.Exists(aot))
                {
                    Directory.Delete(aot,true);
                }
                Directory.CreateDirectory(aot);
                string[] files = Directory.GetFiles(aotSrc);
                foreach (var file in files)
                {
                    var s = Path.GetFileName(file);
                    Debug.Log($"copy {Path.Combine(aotSrc,s)} to {Path.Combine(aot,s+".bytes")}");
                    File.Copy(Path.Combine(aotSrc,s),Path.Combine(aot,s+".bytes"));
                }
                //刷新
                AssetDatabase.Refresh();
            }
            else
            {
                Debug.LogError("文件不存在，拷贝失败");
            }
        }
    }

    /// <summary>
    /// 资源操作事件
    /// </summary>
    public class ProjectEvent : AssetModificationProcessor
    {
        //要添加的注释内容
        private static string annotationStr =
            "using GameClient;\n\n" +
            "namespace UI\n" +
            "{\n" +
            "\tpublic class XXX : UIDialog\n" +
            "\t{\n"+
            "\t\tpublic static void Show()\n"+
            "\t\t{\n" +
            "\t\t\tdialog.Show(\"XXX\",true);\n"+
            "\t\t}\n"+
            "\t}\n" +
            "\n}";

        //创建资源时的回调
        private static void OnWillCreateAsset(string name)
        {
            string path = name.Replace(".meta", "");
            if (path.EndsWith("Dialog.cs")) //弹窗界面 用自定义模板
            {
                GenericMenu genericMenu = new GenericMenu();
                genericMenu.AddItem(new GUIContent("确定"), false,
                    (object o)
                        =>
                    {
                        string fileName = Path.GetFileName(path).Split(".cs")[0];
                        annotationStr = annotationStr.Replace("XXX",
                            fileName);
                        File.WriteAllText(path, annotationStr);
                    }, "Done");
                genericMenu.AddSeparator("");
                genericMenu.AddItem(new GUIContent("取消"), false, () => { return; });
                genericMenu.ShowAsContext();
            }
        }
    }
}