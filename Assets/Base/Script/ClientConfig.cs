using UnityEngine;

namespace Base
{
    public enum Platform
    {
        StandaloneWindows64,
        Android,
        WebGL
    }
    [CreateAssetMenu(fileName = "ClientConfig",menuName = "Config/Client")]
    public class ClientConfig : ScriptableObject
    {
        [Header("热更IP地址")]
        public string HotIpAddress;
        [Header("构建平台")]
        public Platform BuildPlatform;
        [Header("保存数据是否加密")]
        public bool SaveDataEncryption = false;
        [Header("开启socket")]
        public bool StarSocketServer;
        
        [Header("服务器IP地址")]
        public string ServerIpAddress;
    }

}