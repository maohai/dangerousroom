using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Cysharp.Threading.Tasks;
using HybridCLR;
using UnityEngine;
using UnityEngine.UI;
using YooAsset;

namespace Base
{
    public class Loading : MonoBehaviour
    {
        [SerializeField] private Text txtTips;

        [SerializeField] private Slider progressSlider;

        [SerializeField] private Image imgBg;
        
        private long downloadSize;
        private long totalSize;

        private async void Start()
        {
            YooAssetManager.Ins.EventDownloadUpdate += OnDownloadUpdate;
            StartProgress();
            YooAssets.Initialize();
            await YooAssetManager.LoadPackage("Main");
            UpdateCompile();
        }

        private async void UpdateCompile()
        {
            Debug.Log("所有都完成");

            await LoadAOT();
            await LoadStartDll();
            Destroy(transform.parent.gameObject);
        }
        
        private async UniTask LoadStartDll()
        {
#if UNITY_EDITOR
            Assembly clientModule =
                AppDomain.CurrentDomain.GetAssemblies().First(a => a.GetName().Name == "ClientModule");
#else
            var package =await YooAssetManager.LoadPackage("Main");
            var hr = package.LoadRawFileAsync("ClientModule.dll");
            await hr;
            var clientModule =Assembly.Load(hr.GetRawFileData());
#endif
            clientModule.GetType("GameClient.Client").GetMethod("Run").Invoke(null, null);
        }

        private async UniTask LoadAOT()
        {
#if !UNITY_EDITOR
            var package =await YooAssetManager.LoadPackage("Main");
            var  infos = package.GetAssetInfos("AOT");
            foreach (var assetInfo in infos)
            {
                var a = package.LoadRawFileAsync(assetInfo);
                await a;
                RuntimeApi.LoadMetadataForAOTAssembly(a.GetRawFileData(), HomologousImageMode.SuperSet);
            }
#endif
        }

        /// <summary>
        /// 开启进度
        /// </summary>
        private void StartProgress()
        {
            txtTips.text = "";
            progressSlider.value = 0;
            imgBg.color = Color.white;
            progressSlider.gameObject.SetActive(true);
        }
        
        private float progress;
        private void Update()
        {
            if (totalSize == 0||!progressSlider.gameObject.activeSelf)
            {
                // progressSlider.value = 1;
                return;
            }
            txtTips.text = (downloadSize/1048576) + "MB/" + (totalSize/1048576)+"MB";
            float p = downloadSize * 1f / totalSize;
            progress = Mathf.Lerp(progress, p, 2 * Time.deltaTime);
            progressSlider.value = progress;
        }

        private void OnDownloadUpdate(long cur,long total)
        {
            downloadSize = cur;
            totalSize = total;
        }

        private void OnDestroy()
        {
            YooAssetManager.Ins.EventDownloadUpdate -= OnDownloadUpdate;
            YooAssetManager.UnloadAssets("Main");
        }
    }
}