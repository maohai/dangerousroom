using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>, new(){
    protected static T ins;

    public static T Ins{
        get{
            if (ins == null)
                ins = new T();
            return ins;
        }
    }

    protected virtual void Awake(){
        ins = this as T;
    }
    protected virtual void OnDestroy() {
        if (ins == this) {
            ins = null;
        }
    }
}
