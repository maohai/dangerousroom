using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour {
    [SerializeField] Pool[] playerProjectilePools;//对象池数组

    //Pool就是一个对象池
    static Dictionary<GameObject, Pool> dictionary;
    private void Start() {
        dictionary = new Dictionary<GameObject, Pool>();
        Initialize(playerProjectilePools);
    }
#if UNITY_EDITOR
    private void OnDestroy() {//会在游戏编辑器游戏停止时候自动调用
        CheckPoolSize(playerProjectilePools);
    }
#endif
    void CheckPoolSize(Pool[] pools) {//发出警告
        foreach (var pool in pools) {
            if (pool.RuntimeSize > pool.Size)
                Debug.LogWarning(
                    string.Format("Pool:{0} has a runtime size {1} bigger than its initial size {2}",
                    pool.Prefab.name,
                    pool.RuntimeSize,
                    pool.Size)
                    );
        }
    }
    void Initialize(Pool[] pools){
        int i = 0;
        foreach (var pool in pools){
            i++;
#if UNITY_EDITOR //只有unity会报错，其他平台不得行，帮助debug用
            if (dictionary.ContainsKey(pool.Prefab)) {
                Debug.LogError("存在相同的预制体" + pool.Prefab.name);
                continue;//存在相同预制体
            }
#endif
            dictionary.Add(pool.Prefab, pool);//key不同，值相同
            Transform poolParent = new GameObject("Pool:" + pool.Prefab.name).transform;
            poolParent.parent = transform;
            pool.Initialize(poolParent);
        }
    }
    public static GameObject Release(GameObject prefab) {//释放出一个预制体
#if UNITY_EDITOR
        if (!dictionary.ContainsKey(prefab)) {
            Debug.LogError("找不到" + prefab.name);
            return null;
        }
#endif
        //将对应的池中预制件取出
        return dictionary[prefab].PrepareObject();
    }
    public static GameObject Release(GameObject prefab, Vector3 pos) {//释放出一个预制体
#if UNITY_EDITOR
        if (!dictionary.ContainsKey(prefab)) {
            Debug.LogError("找不到" + prefab.name);
            return null;
        }
#endif
        //将对应的池中预制件取出
        return dictionary[prefab].PrepareObject(pos);
    }
    public static GameObject Release(GameObject prefab, Vector3 pos, Quaternion rotation) {//释放出一个预制体
#if UNITY_EDITOR
        if (!dictionary.ContainsKey(prefab)) {
            Debug.LogError("找不到" + prefab.name);
            return null;
        }
#endif
        //将对应的池中预制件取出
        return dictionary[prefab].PrepareObject(pos, rotation);
    }
    public static GameObject Release(GameObject prefab, Vector3 pos, Quaternion rotation, Vector3 localScale) {//释放出一个预制体
#if UNITY_EDITOR
        if (!dictionary.ContainsKey(prefab)) {
            Debug.LogError("找不到" + prefab.name);
            return null;
        }
#endif
        //将对应的池中预制件取出
        return dictionary[prefab].PrepareObject(pos, rotation, localScale);
    }
}