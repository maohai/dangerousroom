using System;
using System.Collections;
using UnityEngine;


public class AutoDeactivate : MonoBehaviour
{
    [SerializeField] bool destroyGameObject;
    [SerializeField] float lifetime = 3f;

    WaitForSeconds waitForSeconds;
    private void Awake() {
        waitForSeconds = new WaitForSeconds(lifetime);
    }

    private void OnEnable(){
        StartCoroutine(nameof(DeactiveCoroutine));
    }

    public void Hide(){
        if (destroyGameObject)
            Destroy(gameObject);
        else gameObject.SetActive(false);
    }
    public IEnumerator DeactiveCoroutine() {
        yield return waitForSeconds;
        Hide();
    }
}