using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Game{
    public interface IAttack{
        /// <summary>
        /// 实现攻击
        /// </summary>
        /// <param name="attack"></param>
        public void Attack(List<GameObject> objs);
    }
}
