using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using GameClient;
using UnityEngine;
using CharacterInfo = Game.CharacterInfo;
namespace Game{
    public class CharacterPlayer :BaseCharacter,IAttack{
        public CharacterInfo info;

        private void Awake(){
            info = Client.Ins.Character.GetInfoByID(1);
            Init(info);
        }

        public override void Init(CharacterInfo info){
            base.Init(info);
        }

        private void Update(){
            if (Input.GetKeyDown(KeyCode.Alpha1)){
                Demage(1);
            }
        }

        public void Attack(List<GameObject> objs){
            foreach (var o in objs){
                if (o.TryGetComponent<BaseCharacter>(out var data)){
                    Debug.Log("攻击");
                }
            }
        }

        public void Demage(int demage){
            info.Data.HP -= demage;
        }
    }

}