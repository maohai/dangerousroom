using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;


namespace Game {
    public interface BaseModule {
        void Init(JObject db, JObject json, Action cb);
        void Ready();
        void Destroy();
		
        string ToJsonKey();
        JObject ToJsonValue();
    }
}