using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Game{
    public class PlayerModule : Singleton<PlayerModule>, BaseModule{

        public void Init(JObject db, JObject json, Action cb){
            cb?.Invoke();
        }

        public void Ready(){
            
        }

        public void Destroy(){
            
        }

        public string ToJsonKey(){
            return "Player";
        }

        public JObject ToJsonValue(){
            return null;
        }
    }
}
