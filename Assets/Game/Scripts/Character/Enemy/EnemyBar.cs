using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyBar : MonoBehaviour{
    private Transform TransFollow;

    public Image ImgHP;
    public Text TxtHP;

    public void Init(Transform t,float n,float max){
        TransFollow = t;
        
        TxtHP.text = $"{n}";
        ImgHP.rectTransform.sizeDelta =
            new Vector2(ImgHP.transform.parent.rectTransform().sizeDelta.x * n/ max,
                ImgHP.transform.parent.rectTransform().sizeDelta.y);
    }

    public void Destroy(){
        Destroy(gameObject);
    }

    private void Update(){
        if (TransFollow != false){
            transform.position = Camera.main.WorldToScreenPoint(TransFollow.position) + new Vector3(0,50,0);
        } 
    }
}
