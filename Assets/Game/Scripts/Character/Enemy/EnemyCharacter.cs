using System;
using System.Collections;
using System.Collections.Generic;
using GameClient;
using UI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game{
    public class EnemyCharacter : BaseCharacter{
        public GameObject HealthBar;
        public GameObject TargetPos;

        
        public CharacterInfo info;
        public const int ID = 2;

        private Coroutine timer;

        private GameObject _healBar;

        private void Awake(){
            info = Client.Ins.Character.GetInfoByID(ID);
        }
        private void OnEnable(){
            CharacterModule.OnEnemyHP.Trigger();
            _healBar = Instantiate(HealthBar,MainDialog.Ins.TransEnemyHP);
            _healBar.GetComponent<EnemyBar>().Init(transform, info.Data.HP, info.MaxHP);
            timer = StartCoroutine(nameof(Move));
        }
        private void Start(){
            Init(info);
        }
        private void OnDisable(){
            if(timer!=null)
                StopCoroutine(timer);
            Destroy(_healBar);
        }

        public override void Init(CharacterInfo info){
            base.Init(info);
        }

        private void OnTriggerEnter2D(Collider2D other){
            if (other.transform.TryGetComponent<AutoDeactivate>(out var d)){ 
                d.Hide();
            }
        }

        private IEnumerator Move(){
            var playerPos = GameObject.Find("Player").transform.position;
            var newPosition = Quaternion.Euler(Random.Range(0,360),Random.Range(0,360),0)//偏转方向 
                * Vector3.forward.normalized * 2 //正前方2米 
                + playerPos; //当前位置
            
            yield return StartCoroutine(MoveTarget(newPosition));
            StartCoroutine(nameof(Move));
        }
        
        private IEnumerator MoveTarget(Vector2 target){
            while (Vector2.Distance(transform.position, target) >= 0.1f){
                transform.position = Vector2.MoveTowards(transform.position, target, 5 * Time.deltaTime);
                yield return null;
            }
        }
    }
}
