using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game;
using GameClient;

namespace Game{
    public class BaseCharacter : MonoBehaviour{
        public CharacterInfo info;

        public virtual void Init(CharacterInfo info){
            this.info = info;
        }
    }
}
