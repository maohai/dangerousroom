using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Unity.VisualScripting;
using UnityEngine;

namespace Game{
    public class Projectile : MonoBehaviour
    {
        public float moveSpeed = 0.1f;
        public Vector2 moveDirectly;
        protected GameObject target;//子弹目标
        public float startTime = 0.06f;

        public TrailRenderer _trail;

        private Coroutine timer;

        private void Awake(){
            _trail = GetComponentInChildren<TrailRenderer>();
        }

        protected IEnumerator BeforeEnable(){
            yield return null;
            _trail.Clear(); // 清除轨迹数据
            _trail.emitting = true; // 停止发射新的轨迹
            _trail.enabled = true;
            moveDirectly.x = PlayerInput.Ins.Dir;
        }

        protected virtual void OnEnable(){
            StartCoroutine(nameof(BeforeEnable));
            //每一次游戏对象启用的时候执行 
            Move();
        }

        protected void OnDisable(){
            _trail.emitting = false;
            _trail.enabled = false;
        }

        public void Move(){
            if(timer != null)
                StopCoroutine(timer);
            timer = StartCoroutine(MoveDirectly());
        }

        private IEnumerator MoveDirectly() {
            //当物体在启用条件的时候
            //只要当子弹没有被摧毁或者禁用，那么就会不断的移动
            while (gameObject.activeSelf) {
                //不依赖刚体组件，大大降低了游戏的性能
                transform.Translate(moveDirectly * moveSpeed * Time.deltaTime);
                yield return null;
            }
        }
        
        protected void SetTarget(GameObject target) => this.target = target;
    }
}
