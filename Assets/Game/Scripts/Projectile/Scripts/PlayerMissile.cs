using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///
/// </summary>

namespace Game{
    public class PlayerMissile : PlayerProjectileOverdrive
    {
        // [SerializeField] AudioData targetAcquiredVoice = null;
        [Header("=== SPEED CHANGE ===")]
        [SerializeField] float lowSpeed = 8f;
        [SerializeField] float highSpeed = 25f;
        [SerializeField] float variableSpeedDelay = 0.5f;//慢速持续时间

        WaitForSeconds waitVariableSpeedDelay;

        private void Awake() {
            waitVariableSpeedDelay = new WaitForSeconds(variableSpeedDelay);
        }
        protected override void OnEnable() {
            base.OnEnable();
            StartCoroutine(nameof(VariableSpeedCoroutine));
        }
        IEnumerator VariableSpeedCoroutine() {
            moveSpeed = lowSpeed;
            yield return waitVariableSpeedDelay;
            moveSpeed = highSpeed;
        }
    }
}