using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class PlayerProjectileOverdrive : Projectile
    {
        [SerializeField] ProjectileGuidanceSystem guidanceSystem;
        
        protected override void OnEnable(){
            var tag = GameObject.FindGameObjectWithTag("Player");
            if(tag){
                var g = tag.transform.GetComponent<EnemyCharacter>();
                if(g)
                    SetTarget(g.TargetPos);
            }
            transform.rotation = Quaternion.identity;
            
            
            if (target == null)
                base.OnEnable();
            else{
                StartCoroutine(nameof(BeforeEnable));
      
                StartCoroutine(guidanceSystem.HomingCoroutine(target)); 
            }
        }
    }
}