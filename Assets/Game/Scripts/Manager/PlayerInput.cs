using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Game{
    public class PlayerInput : Singleton<PlayerInput>, InputActions.IPlayerActions{
        public InputActions input;

        public float Dir = 1;
        public event Action onAttack = delegate{  };
        public event Action onStopAttack = delegate{  };

        protected override void Awake(){
            base.Awake();
            input = new InputActions();
            input.Player.SetCallbacks(this); //回调函数，非常重要
        }

        private void OnDisable(){
            DisableAllInput();
        }

        public void DisableAllInput(){
            input.Player.Disable(); //禁用操作
        }

        public void EnablePlayerPlay(){
            input.Player.Enable();
        }

        public void DisablePlayerPlay(){
            input.Player.Disable();
        }
        
        public void OnMove(InputAction.CallbackContext context){
            //当接受的信号为移动信号时 InputActionPhase是一个枚举类型
            //Disabled = 0,禁用时
            //Waiting = 1,被启用时，但是没有响应的信号输入
            //Started = 2,按下按键的那一帧，相当于Input.GetKeyDown()
            //Performed = 3,输入动作已执行的时候，包含了按下未弹起相当于Input.GetKey
            //Canceled = 4,信号停止的时候

            //因为是持续的移动，所以选择Performed
            // if (context.performed || context.started)
                //读取输入动作的二维向量的值
                // onMove.Invoke(context.ReadValue<Vector2>());
                
            // if (context.canceled)
                // onStopMove.Invoke();
        }
        public void OnAttack(InputAction.CallbackContext context){
            if (context.performed)
                onAttack?.Invoke();
            if (context.canceled)
                onStopAttack?.Invoke();
        }

        public void OnSkip(InputAction.CallbackContext context){ }

        public Vector2 Move => input.Player.Move.ReadValue<Vector2>(); //读取移动
        public bool IsMove => Move.x != 0;
        public float MoveX => Move.x;
        public bool Attack => input.Player.Attack.WasPressedThisFrame();
        public bool Skip => input.Player.Skip.WasPressedThisFrame();

    }

}