using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class PlayerManager : Singleton<PlayerManager>{
        PlayerInput input;
        Rigidbody2D rigidbody;
        PlayerGroundedDetector groundedDetector;

        public GameObject Prefab_Enemy;

        [SerializeField] GameObject projectile;//子弹预制件
        [SerializeField] Transform muzzle;//子弹发射位置
        [SerializeField] float fireInteral = 0.12f;//子弹发射间隔
        WaitForSeconds WaitForFireInteral;
        
        private void Awake() {
            input = GetComponent<PlayerInput>();
            rigidbody = GetComponent<Rigidbody2D>();
            groundedDetector = GetComponentInChildren<PlayerGroundedDetector>();
        }

        private void Start() {
            input.EnablePlayerPlay();//启用表
            WaitForFireInteral = new WaitForSeconds(fireInteral);//开火间隔
        }

        private void Update(){
            if (Input.GetMouseButtonDown(1)){
                PoolManager.Release(Prefab_Enemy, transform.position, Quaternion.identity);
            }
        }

        private void OnEnable(){
            input.onAttack += Fire;
            input.onStopAttack += StopFire;
        }

        private void OnDisable(){
            input.onAttack -= Fire;
            input.onStopAttack -= StopFire;
        }


        //设置刚体的速度
        public void SetVelocity(Vector3 velocity) => rigidbody.velocity = velocity;
        //设置刚体X轴的值
        public void SetVelocityX(float velocityX) => rigidbody.velocity = new Vector2(velocityX, rigidbody.velocity.y);
        //设置刚体Y轴上的值
        public void SetVelocityY(float velocityY) => rigidbody.velocity = new Vector2(rigidbody.velocity.x, velocityY);
        //移动函数  传入速度,然后设置(速度*方向上的轴的值)即可
        public void Move(float speed){
            SetVelocityX(speed * input.MoveX);
        }
        //检测是否在地面上
        public bool IsGrounded => groundedDetector.IsGround;
        //当有一个向下的力,而且没在地面上的时候
        public bool IsFalling => rigidbody.velocity.y < 0f && !IsGrounded;
        
        #region FIRE
        void Fire() {
            StartCoroutine(nameof(FireCoroutine));
        }
        void StopFire() {
            //使用这个重载是因为 StopCoroutine(FireCoroutine());可能会不执行
            StopCoroutine(nameof(FireCoroutine));
        }
        private IEnumerator FireCoroutine() {
            while (true) {
                //子弹预制件，位置，不需要旋转
                PoolManager.Release(projectile, muzzle.position, Quaternion.identity);
                //最好不要在while里面new
                yield return WaitForFireInteral;
            }
        }
        #endregion
    }

}