using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    [CreateAssetMenu(menuName = "Data/StateMachine/PlayerState/Attack", fileName = "PlayerState_Attack")]
    public class PlayerState_Attack : PlayerState{
        private float acceleration = 5f;

        public override void LogicUpdate() {
            if (IsAnimationFinished && !input.IsMove) {
                //切换为闲置动作
                stateMachine.SwitchState(typeof(PlayerState_Idle));
            }
            if (input.IsMove) {
                //切换为闲置动作
                stateMachine.SwitchState(typeof(PlayerState_RunA));
            }
            
            //MoveTowards(当前值,目标值,每一帧所变化的值)
            currentSpeed = Mathf.MoveTowards(currentSpeed, 0, acceleration * Time.deltaTime);
            player.Move(currentSpeed);
        }
        public override void PhysicUpdate() {
            // player.Move(currentSpeed);
        }
    }

}