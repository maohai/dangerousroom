using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    [CreateAssetMenu(menuName = "Data/StateMachine/PlayerState/Idle", fileName = "PlayerState_Idle")]
    public class PlayerState_Idle : PlayerState{
        [SerializeField] float deceleration = 1f; //数值越大减的越快

        public override void LogicUpdate(){
            if(input.Skip)
                stateMachine.SwitchState(typeof(PlayerState_Skip));//切换为掉落
            if (!player.IsGrounded)
                stateMachine.SwitchState(typeof(PlayerState_Fall));//切换为掉落
            if (input.Attack)
                stateMachine.SwitchState(typeof(PlayerState_Attack));
            if (input.IsMove){
                //切换为跑步动作
                stateMachine.SwitchState(typeof(PlayerState_Run));
            }

            currentSpeed = Mathf.MoveTowards(currentSpeed, 0, deceleration * Time.deltaTime);
        }

        public override void PhysicUpdate(){
            //因为停止移动的时候没有任何输入,但玩家移动是有左右两个方向,所以得乘上方向
            player.SetVelocityX(currentSpeed * player.transform.localScale.x);
        }
    }
}