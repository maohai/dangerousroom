using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    [CreateAssetMenu(menuName = "Data/StateMachine/PlayerState/Down", fileName = "PlayerState_Down")]
    public class PlayerState_Down : PlayerState{
        public override void Enter() {
            base.Enter();
            player.SetVelocity(Vector3.zero);
        }
        public override void LogicUpdate() {
            if (input.Skip)
                stateMachine.SwitchState(typeof(PlayerState_Skip));
            if (input.IsMove)
                stateMachine.SwitchState(typeof(PlayerState_Run));
            if (IsAnimationFinished)//当掉落完成的时候
                stateMachine.SwitchState(typeof(PlayerState_Idle));
        }
    }

}