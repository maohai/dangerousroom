using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    [CreateAssetMenu(menuName = "Data/StateMachine/PlayerState/Skip", fileName = "PlayerState_Skip")]
    public class PlayerState_Skip : PlayerState{
        [SerializeField] float jumpForce = 7f;
        [SerializeField] float speedMove = 5f;
        public override void Enter() {
            base.Enter();
            player.SetVelocityY(jumpForce);
        }
        public override void LogicUpdate() {
            if (player.IsFalling)
                stateMachine.SwitchState(typeof(PlayerState_Fall));
        }
        public override void PhysicUpdate() {
            player.Move(speedMove);
        }
    }
}