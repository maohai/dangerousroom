using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    [CreateAssetMenu(menuName = "Data/StateMachine/PlayerState/Run", fileName = "PlayerState_Run")]
    public class PlayerState_Run : PlayerState
    {
        [SerializeField] float runSpeed = 5f;
        [SerializeField] float acceleration = 2f;
        public override void Enter() {
            base.Enter();
            currentSpeed = 10;
        }
        public override void LogicUpdate() {
            if(input.Skip)
                stateMachine.SwitchState(typeof(PlayerState_Skip));//切换为掉落
            
            if (!player.IsGrounded)
                stateMachine.SwitchState(typeof(PlayerState_Fall));//切换为掉落

            if (!input.IsMove) {
                //切换为闲置动作
                stateMachine.SwitchState(typeof(PlayerState_Idle));
            }

            if (input.Attack){
                stateMachine.SwitchState(typeof(PlayerState_RunA));
            }
            
            //MoveTowards(当前值,目标值,每一帧所变化的值)
            var x = input.MoveX;
            
            if (player.transform.localScale.x == 1 && x < 0)
                player.transform.localScale = new Vector3(-1, 1, 1);
            else if (player.transform.localScale.x == -1 && x > 0)
                player.transform.localScale = new Vector3(1, 1, 1);
            input.Dir = player.transform.localScale.x;
        
            currentSpeed = Mathf.MoveTowards(currentSpeed, runSpeed, acceleration * Time.deltaTime);

        }
        public override void PhysicUpdate() {
            player.Move(currentSpeed);
        }
    }

}