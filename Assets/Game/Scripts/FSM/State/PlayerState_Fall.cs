using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    [CreateAssetMenu(menuName = "Data/StateMachine/PlayerState/Fall", fileName = "PlayerState_Fall")]
    public class PlayerState_Fall : PlayerState{
        [SerializeField] AnimationCurve speedCurve;//动画曲线变量
        [SerializeField] float speedMove = 5f;
        public override void LogicUpdate() {
            if (player.IsGrounded)
                stateMachine.SwitchState(typeof(PlayerState_Down));
        }
        public override void PhysicUpdate() {
            player.Move(speedMove);
            //根据曲线来变化Y轴的值
            player.SetVelocityY(speedCurve.Evaluate(StateDuration));
        }
    }

}