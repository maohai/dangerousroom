using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game{
    public class PlayerStateMachine:StateMachine//将挂载到玩家身上
    {
        [SerializeField] PlayerState[] states;

        Animator animator;
        PlayerInput input;
        PlayerManager player;

        private void Awake() {
            animator = GetComponentInChildren<Animator>();
            input = GetComponent<PlayerInput>();
            player = GetComponent<PlayerManager>();
            stateTable = new Dictionary<System.Type, IState>(states.Length);//初始化表

            foreach (var state in states) {
                state.Initialize(animator, input,player,this);
                stateTable.Add(state.GetType(), state);//GetType()  获取类型,因为类型是唯一的
            }
        }
        private void Start() {
            SwitchOn(stateTable[typeof(PlayerState_Idle)]);
        }

    }
}

