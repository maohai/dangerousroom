using System;
using GameClient;
using GameClient.NetWork;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class LoginDialog : UIAniDialog
    {
        public static void Show()
        {
            dialog.Show("LoginDialog", false);
        }

        public InputField inputName;
        public InputField inputPassword;
        public InputField InputConfirmPassword;

        protected override void OnUIShow(params object[] param)
        {
            base.OnUIShow(param);
        }

        protected override void OnUIShowCompile()
        {
            base.OnUIShowCompile();
            
            Close(ChatDialog.Show);
        }

        /// <summary>
        /// 注册
        /// </summary>
        public void OnBtnRegisterClick()
        {
            string name = inputName.text;
            string password = inputPassword.text;
            string confirmPassword = InputConfirmPassword.text;
            if (name.Length < 3)
            {
                ShowTips("账户不能少于3个字符");
                return;
            }

            if (password.Length < 5)
            {
                ShowTips("密码不能少于5个字符");
                return;
            }

            if (confirmPassword != password)
            {
                ShowTips("前后密码不一致");
                return;
            }

            RegisterInfoDto dto = new RegisterInfoDto()
            {
                account = name,
                password = password
            };

            Client.Ins.Player.Register(dto, () => { });
        }
    }
}