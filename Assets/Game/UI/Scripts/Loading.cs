using System;
using System.IO;
using Base;
using GameClient;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class Loading : MonoBehaviour
    {
        [SerializeField] private Text txtTips;

        [SerializeField] private Slider progressSlider;

        private float progress = 0;

        private void Start()
        {
            txtTips.text = "0%";
            progressSlider.value = 0;
        }

        private void Update()
        {
            float p = Client.Ins.GetLoadProgress();
            progress = Mathf.Lerp(progress, p, 2*Time.deltaTime);
            txtTips.text = Math.Round(progress,2)*100+"%";
            progressSlider.value = progress;
            if (progress >= 0.95f&&p>=1)
            {
                LoadCompile();
            }
        }

        private async void LoadCompile()
        {
            Destroy(gameObject);
            var package = await YooAssetManager.LoadPackage("Main");
            package.LoadSceneAsync("Game");
            MainDialog.Show();
        }
    }
}