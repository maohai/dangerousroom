using System;
using Game;
using GameClient;
using UnityEngine;
using UnityEngine.UI;
using CharacterInfo = Game.CharacterInfo;

namespace UI
{
	public class MainDialog : SingleUIDialog<MainDialog>{
		private CharacterInfo info;
		
		public ShowHP HP;
		public ShowHP MP;

		public Text TxtAttack;

		public Transform TransEnemyHP;
		public static void Show()
		{
			dialog.Show("MainDialog",false);
		}

		private void Update(){
			if (Input.GetKeyDown(KeyCode.Alpha2)){
				RefreshUI();
			}
		}

		protected override void OnUIShow(params object[] param){
			base.OnUIShow(param);
			RefreshUI();
		}

		public void RefreshUI(){
			info = Client.Ins.Character.GetInfoByID(1);
			HP.Refresh(info.Data.HP,info.MaxHP);
			MP.Refresh(info.Data.MP,info.MaxMP);
			TxtAttack.text = $"A:{info.Data.Attack}";
		}
	}
}