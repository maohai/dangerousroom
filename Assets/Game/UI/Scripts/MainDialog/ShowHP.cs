using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowHP : MonoBehaviour
{
    public Image ImgHP;
    public Text TxtHP;

    public void Refresh(float n,float max){
        TxtHP.text = $"{n}";
        ImgHP.rectTransform.sizeDelta =
            new Vector2(ImgHP.transform.parent.rectTransform().sizeDelta.x * n/ max,
                ImgHP.transform.parent.rectTransform().sizeDelta.y);
    }
    
}
