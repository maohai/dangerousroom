@echo off
set folderPath="../Assets/Client/Scripts/Data"

if exist %folderPath% (
    echo Folder exists, deleting...
    rmdir /s /q %folderPath%
)

echo Creating new folder...
mkdir %folderPath%

python ./main.py ../Assets/Client/Scripts/Data ../Assets/Client/Resources 1