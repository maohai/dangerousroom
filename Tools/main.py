import os
try:
    from openpyxl import load_workbook
except:
    print("没有")
    os.system("pip install openpyxl")
    from openpyxl import load_workbook

import sys

path="Data.json"

dirPath=""
isCreateCs=True
# print(sys.argv)
if len(sys.argv)==4:
    dirPath=sys.argv[1]
    path=os.path.join(sys.argv[2],path)
    if sys.argv[3]!="1":
        isCreateCs=False


wb=load_workbook("./数值表.xlsx")
allSheet = wb.sheetnames
data={}


cs="""
using Newtonsoft.Json.Linq;

namespace GameClient.Data
{
    public class NMAE
    {
ATTRIBUTES
        public NAMEFUN(JToken data)
        {
INIT
        }
    }
}
"""

LString='''
namespace GameClient.Data
{
    public class Lstring
    {
    
LSTRING

    }
}
'''
csData={}
def createData(sheetName):
    t=wb[sheetName]
    if t.max_row<=2:
        return
    data[sheetName]=[]
    ls=""
    for y in range(2,t.max_row+1):
        if y==3:#第三行是数据类型
            continue
        s=[]
        for x in range(1,t.max_column+1):
            s.append(t.cell(y,x).value)
        data[sheetName].append(s)

        if sheetName=="Localization" and y>3:#本地化处理
            l="\t\t/// <summary>\n\t\t/// {0}\n\t\t/// <summary>\n\t\tpublic const string {1}=\"{1}\";".format(str(t.cell(y,2).value),str(t.cell(y,1).value))
            if y<t.max_row:
                l+="\n"
            ls+=l


    if ls!="":
        ls = LString.replace("LSTRING",ls)
        p=os.path.join(dirPath,"Lstring.cs")
        with open(p,"w",encoding="utf-8") as file:
            file.write(ls)
        

    if sheetName!="Localization" and isCreateCs:#非本地化
        c=cs
        name=sheetName+"Data"
        attributes=""
        initStr=""
        for x in range(1,t.max_column+1):
            attrName=str(t.cell(2,x).value)
            attrType=str(t.cell(3,x).value)
            attrDsc=str(t.cell(1,x).value)

            attr1="\t\t/// <summary>\n\t\t/// {0}\n\t\t/// </summary>\n".format(attrDsc)
            attr2="\t\tpublic {0} {1}".format(attrType,attrName)+"{ get; set; }\n"

            init="\t\t\t{0} = data[\"{0}\"].ToObject<{1}>();".format(attrName,attrType)
            #数组处理
            if '[' in attrType:
                init="\t\t\t{0} = data[\"{0}\"].ToCustomArray<{1}>();".format(attrName,attrType.split("[")[0])

            if x<t.max_column:
                init+="\n"

            attributes+=attr1+attr2
            initStr+=init
        
        c = c.replace("NMAE",name).replace("NAMEFUN",name).replace("ATTRIBUTES",attributes).replace("INIT",initStr)
        p=os.path.join(dirPath,(name+".cs"))
        with open(p,"w",encoding="utf-8") as file:
            file.write(c)
        
            
    print(n+" 完成")



for n in allSheet:
    createData(n)


print("输出路径"+path) 

with open(path,'w',encoding="utf-8") as file:
    s = str(data).replace('\'','\"')
    file.write(s)

